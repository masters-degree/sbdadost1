<?php
require_once 'core/init.php';
$redirector = Redirector::getInstance();
if ($redirector->headersRequired()) {
   ?>
   <!DOCTYPE HTML>
   <html lang='en-us'>
      <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
         <title>Supermercados All-in</title>
         <!-- CSS  -->
         <link href="<?= BASE_URL ?>/assets/css/icons.css" rel="stylesheet">
         <link href="<?= BASE_URL ?>/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
         <link href="<?= BASE_URL ?>/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
         <link href="<?= BASE_URL ?>/assets/css/general.css" type="text/css" rel="stylesheet"/>
         <link rel="icon" href="<?= BASE_URL ?>/assets/images/favicon.ico"/>
         <!--  Scripts-->
         <script src="<?= BASE_URL ?>/assets/js/jquery.min.js" ></script>
         <script src="<?= BASE_URL ?>/assets/js/materialize.min.js" ></script>
         <script src="<?= BASE_URL ?>/assets/js/general.js" ></script>
         <script>baseURL = '<?= BASE_URL; ?>';</script>
      </head>
      <body>
         <?php
      }
      $redirector->start();
      if ($redirector->headersRequired()) {
         ?>
      </body>
   </html>
   <?php
}
