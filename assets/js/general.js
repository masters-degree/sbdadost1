$(document).ready(function () {
    $('select').material_select();
    $('.tooltipped').tooltip({delay: 50});
    $('.collapsible').collapsible({
        accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

    // Caso enter seja pressionado na tela de login, performa validação do formulário de login
    $("#formLogin").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            validateFormLogin();
        }
    });

    var loadingContainer = '<div id="loading-container">' +
            '<div id="loading">' +
            '<div class="col s12 m12">' +
            ' <div class="card-panel cyan darken-2">' +
            '<div class="progress">' +
            '<div class="indeterminate"></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div>';

    $(loadingContainer).appendTo('body');

    var $loading = $('#loading-container').hide();

    //Attach the event handler to any element
    $(document)
            .ajaxStart(function () {
                //ajax request went so show the loading image
                $loading.show();
            })
            .ajaxStop(function () {
                //got response so hide the loading image
                $loading.hide();
            });
    setInterval(updateNotification(), 60000);
});

function validateFormRegister() {

    var error = false;

    // input firstname
    var fistName = $('#firstName').val();
    $("#firsNameMessage").removeClass();
    if (isEmpty(fistName)) {
        //seta mensagem ok
        $("#firstNameMessage").addClass('success').text('OK');
    } else {
        //seta mensagem erro
        error = true;
        $("#firstNameMessage").addClass('error').text('Nome não pode ficar em branco');
    }

    // input firstname
    var lastName = $('#lastName').val();
    $("#lastNameMessage").removeClass();
    if (isEmpty(lastName)) {
        //seta mensagem ok
        $("#lastNameMessage").addClass('success').text('OK');
    } else {
        //seta mensagem erro
        error = true;
        $("#lastNameMessage").addClass('error').text('Sobrenome não pode ficar em branco');
    }

    // email e numero
    var emailNumber = $('#emailNumber').val();
    if (isEmail(emailNumber) || isNumero(emailNumber)) {
        $("#emailNumberMessage").addClass('success').text('OK');
    } else {
        error = true;
        $("#emailNumberMessage").addClass('error').text('Email ou número inválido');
    }

    // email e numero de novo
    var emailNumberAgain = $('#emailNumberAgain').val();
    if (isEmail(emailNumberAgain) || isNumero(emailNumberAgain)) {
        $("#emailNumberAgainMessage").addClass('success').text('OK');
    } else {
        error = true;
        $("#emailNumberAgainMessage").addClass('error').text('Email ou número inválido');
    }

    if ((isEmail(emailNumber) && isEmail(emailNumberAgain)) || (isNumero(emailNumber) && isNumero(emailNumberAgain))) {
        if (emailNumber !== emailNumberAgain) {
            error = true;
            $("#emailNumberMessage").removeClass().addClass('error').text('Email ou número não são iguais');
            $("#emailNumberAgainMessage").removeClass().addClass('error').text('Email ou número não são iguais');
        }
    } else {
        error = true;
        $("#emailNumberMessage").removeClass().addClass('error').text('Email ou número incompatíveis');
        $("#emailNumberAgainMessage").removeClass().addClass('error').text('Email ou número incompatíveis');
    }

    // input password
    var password = $('#password').val();
    $("#passwordMessage").removeClass();
    if (isEmpty(password)) {
        //seta mensagem ok
        $("#passwordMessage").addClass('success').text('OK');
    } else {
        //seta mensagem erro
        error = true;
        $("#passwordMessage").addClass('error').text('Senha não pode ficar em branco');
    }

    // selects aniversário
    var birthDay = $('#birthDay').val();
    var birthMonth = $('#birthMonth').val();
    var birthYear = $('#birthYear').val();
    $("#birthMessage").removeClass();
    //Select esta com valor padrao
    if (birthDay == null || birthMonth == null || birthYear == null) {
        //seta mensagem erro
        error = false;
        $("#birthMessage").addClass('error').text('Selecione uma data');
    } else {
        //seta mensagem ok
        $("#birthMessage").addClass('success').text('OK');
    }

    // radio genero
    if ($('#male').is(':checked') || $('#female').is(':checked')) {
        //seta mensagem ok
        $("#genderMessage").addClass('success').text('OK');
    } else {
        error = true;
        $("#genderMessage").addClass('error').text('Selecione um gênero');
    }

    if (error) {
        Materialize.toast('Preencha os dados corretamente', 1000, 'red');
    } else {
        registerUser();
    }
}

function validateFormLogin() {
    var error = false;

    // email e numero
    var emailNumber = $('#loginEmailNumber').val();
    $("#loginEmailNumberMessage").removeClass();
    if (isEmail(emailNumber) || isNumero(emailNumber)) {
        $("#loginEmailNumberMessage").addClass('success').text('');
    } else {
        error = true;
        $("#loginEmailNumberMessage").addClass('error').text('Email ou número inválido');
    }
    // input password
    var password = $('#loginPassword').val();
    $("#loginPasswordMessage").removeClass();
    if (isEmpty(password)) {
        //seta mensagem ok
        $("#loginPasswordMessage").addClass('success').text('');
    } else {
        //seta mensagem erro
        error = true;
        $("#loginPasswordMessage").addClass('error').text('Senha não pode ficar em branco');
    }
    if (error) {
        Materialize.toast('Preencha os dados corretamente', 1000, 'red');
    } else {
        var result = $('#formLogin').submit();
    }

}

function registerUser() {
    var result = $('#formUserRegistration').submit();
    if (result) {
        Materialize.toast('Cadastro realizado com sucesso!', 2000, 'green', function () {
//            Materialize.toast('Atualizando página..', 2000, 'light-blue darken-4', function () {
//                window.location.href = window.location.href;
//            })
        });
    } else {
        Materialize.toast('Não foi possível realizar o cadastro!', 4000, 'red');
        return false;
    }

}

function sendData(url, data) {
    Materialize.toast('Enviando dados..', 1000, 'orange accent-4');
    $.ajax({
        type: 'POST',
        url: url,
        data: data
    }).done(flag = function (retorno) {
        if (retorno == "OK") {
            return true;
        } else {
            return false;
        }
    });
}

function login() {
    var data = $('#login-form').serialize();
    sendData(data);
}

function entry() {
    $('#login-modal').openModal();
//    Materialize.toast('Não foi possível completar sua requisição', 1000, 'orange accent-4');
}

function goToRegister() {
    $('#login-modal').closeModal();
    $('#firstName').focus();
}

function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isNumero(valor) {
    if (isNaN(valor) || valor <= 0) {
        return false;
    } else {
        return true;
    }
}

function isEmpty(value) {
    if (value == null || value == "") {
        return false;
    }
    return true;
}

/**
 * Mostra mensagem na tela do usuário
 * @param {type} message Mensagem ou conteúdo hmtl a ser mostrado
 * @param {type} duration Tempo em mls
 * @param {type} classes classes css para formatação do toast
 * @example alertMessage("Erro 501", 4000, 'rounded red')
 * @returns {undefined}1
 */
function alertMessage(message, duration, classes) {
    Materialize.toast(message, duration, classes);
}


function manageNotification(notificationId, url) {
    $.ajax({
        'url': baseURL + "/notifications/ajax",
        'type': 'post',
        'dataType': 'json',
        'data': {
            'action': 'marcarLido',
            'notificationId': notificationId
        }
    }).done(function (response) {
        if (response != false) {
            var badge = $("#badge-notifications").val();
            if (badge >= 1) {
                $("#badge-notifications").val(badge--);
                $("#badge-notifications").show();
            } else {

                $("#badge-notifications").hide();
                $("#badge-notifications").val(0);
            }
            window.location.assign(url);
        }
    });
}
function updateNotification() {
    $.ajax({
        'url': baseURL + "/notifications/ajax",
        'type': 'post',
        'dataType': 'json',
        'data': {
            'action': 'buscar'
        }

    }).done(function (response) {
        if (response != false) {
            for (notification in response) {
                console.log(notification);
//              var notifications = $("#a-notification-sample");

            }
//            var badge = $("#badge-notifications").val();
//            if (badge >= 1) {
//                $("#badge-notifications").val(badge--);
//                $("#badge-notifications").show();
//            } else {
//
//                $("#badge-notifications").hide();
//                $("#badge-notifications").val(0);
//            }
        }
    });
}
function voceQuisDizer(palavra) {
    $.ajax({
        'url': baseURL + "/spellCorrector/ajax",
        'type': 'post',
        'dataType': 'json',
        'data': {
            'action': 'vocequisdizer',
            'keyword': palavra
        }

    }).done(function (response) {
        if (response) {
            $("#vocequisdizer").text(response);
            $("#vocequisdizer").attr({'href': baseURL + '/items/search/' + response});
            $('#container-vocequisdizer').slideDown( "slow" );
        } else {
            $('#container-vocequisdizer').hide();
        }
    }
    );

}
function gotoItems() {
    window.location.href = baseURL + '/items/search/' + $('#products-search').val();
}
