<?php

/**
 * Classe de configuração do projeto singleton
 */
class Config {

   /**
    * @var config Unica instancia da classe - singleton 
    */
   private static $instance;

   /**
    * @var boolean Variável para debug do projeto
    */
   private $debug = false;

   /**
    * @var int Nível de exibição de erros 1,2,3
    */
   private $errorsLevel;

   /**
    * @var string Raiz do projeto no servidor 
    * @example /var/www/foo
    */
   private $projectRoot = '/var/www/all-in';

   /**
    * @var string Host da base de dados
    */
   private $dbHost = 'localhost';

   /**
    * @var string Nome da base de dados
    */
   private $dbDatabase = 'all-in';

   /**
    * @var string Nome de usuário da base de dados
    */
   private $dbUsername = 'root';

   /**
    * @var string Senha da base de dados
    */
   private $dbPassword = 'root';

   /**
    * @var String URL do projeto
    * @example 'http://localhost/api.foo'
    */
   private $baseURL = 'http://local.allin';

   /**
    * @var String Formato de timezone padrao do projeto
    */
   private $defaultTimeZone = 'America/Sao_Paulo';

   /**
    * @var Array Páginas que não necessitam de autorização do login
    */
   private $noAuthorizationRequired = array(
      'index/index',
   );

   /**
    * @var Array Métodos/Actions que não necessitam de headers (apresentação com retorno puro)
    */
   private $noHeadersRequired = array(
      'ajax'
   );

   /**
    * @var int  Número máximo de resultados trazidos pelas consultas
    */
   private $querysPaginationIncrement = 9999999;

   /**
    *
    * @var string Idioma no formato idioma_PAIS. Ex.: pt_BR 
    */
   private $defaultLanguage = 'pt_BR';

   private function __construct() {
      $this->setErrorsLevel(1);
   }

   /**
    * Retorna a mesma instancia da classe para todo o projeto
    * @return Config
    */
   public static function getInstance() {
      if (!isset(self::$instance)) {
         self::$instance = new Config();
      }
      return new self::$instance;
   }

   public function getProjectRoot() {
      $this->projectRoot = str_replace(DIRECTORY_SEPARATOR, "/", $this->projectRoot);
      return $this->projectRoot . DIRECTORY_SEPARATOR;
   }

   public function getErrorsLevel() {
      return $this->errorsLevel;
   }

   /**
    * Ajusta nível de exibição de erros
    * 1 = production environment
    * 2 = developing
    * 3 = hard debugging
    * @param int $errors_level Nível de erro de 1 a 3
    */
   public function setErrorsLevel($errors_level) {
      $this->errorsLevel = $errors_level;
      switch ($this->errorsLevel) {
         case 1:
            ini_set('display_errors', false);
            error_reporting(0); //Produção
            break;
         case 2:
            ini_set('display_errors', true);
            error_reporting(E_ALL ^ E_NOTICE); //Desenvolvimento
            break;
         case 3:
            ini_set('display_errors', true);
            error_reporting(E_ALL); //Hard Debugging
            break;
      }
   }

   /**
    * Exibe mensagem formatada para o desenvolvedor
    * @param string $message Mensagem para exibição
    */
   public function debugMessage($message) {
      if ($this->getDebug()) {
         echo "<pre>" . $message . "</pre>";
      }
   }

   public function getDebug() {
      return $this->debug;
   }

   public function getDbHost() {
      return $this->dbHost;
   }

   public function getDbName() {
      return $this->dbDatabase;
   }

   public function getDbUsername() {
      return $this->dbUsername;
   }

   public function getDbPassword() {
      return $this->dbPassword;
   }

   public function getBaseURL() {
      return $this->baseURL;
   }

   public function getDefaultTimeZone() {
      return $this->defaultTimeZone;
   }

   public function getNoAuthorizationRequired() {
      return $this->noAuthorizationRequired;
   }

   public function setDebug($debug) {
      $this->debug = $debug;
      return $this;
   }

   public function setDbHost($dbHost) {
      $this->dbHost = $dbHost;
      return $this;
   }

   public function setDbName($dbDatabase) {
      $this->dbDatabase = $dbDatabase;
      return $this;
   }

   public function setDbUsername($dbUsername) {
      $this->dbUsername = $dbUsername;
      return $this;
   }

   public function setDbPassword($dbPassword) {
      $this->dbPassword = $dbPassword;
      return $this;
   }

   public function setBaseURL($baseURL) {
      $this->baseURL = $baseURL;
      return $this;
   }

   public function setDefaultTimeZone(String $defaultTimeZone) {
      $this->defaultTimeZone = $defaultTimeZone;
      return $this;
   }

   public function setNoAuthorizationRequired(Array $noAuthorizationRequired) {
      $this->noAuthorizationRequired = $noAuthorizationRequired;
      return $this;
   }

   public function getQuerysPaginationIncrement() {
      return $this->querysPaginationIncrement;
   }

   public function setQuerysPaginationIncrement($querysPaginationIncrement) {
      $this->querysPaginationIncrement = $querysPaginationIncrement;
      return $this;
   }

   public function getDefaultLanguage() {
      return $this->defaultLanguage;
   }

   public function setDefaultLanguage($defaultLanguage) {
      $this->defaultLanguage = $defaultLanguage;
      return $this;
   }

   public function getNoHeadersRequired() {
      return $this->noHeadersRequired;
   }

   public function setNoHeadersRequired(Array $noHeadersRequired) {
      $this->noHeadersRequired = $noHeadersRequired;
      return $this;
   }

}
