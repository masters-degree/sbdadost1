<?php
/**
 * Arquivo para includes e requires de arquivos que serão utilizados no contexto global do projeto
 */
if(php_sapi_name() != 'cli'){
   session_start();
}
/** Inclui a classe de configuração * */
require_once 'config.php';
$config = config::getInstance();
$_SESSION['lang'] = $config->getDefaultLanguage();
date_default_timezone_set($config->getDefaultTimeZone());

/** Define constantes para facilitar inclusão de arquivos * */
define('ROOT', $config->getProjectRoot());
define('BASE_URL', $config->getBaseURL());
define('DEBUG', $config->getDebug());
define('SERVICES', 'services/');

/** Inclui a classe model geral * */
require_once ROOT . '/core/model.php';

/** Inclui a classe controller geral * */
require_once ROOT . '/core/controller.php';

/** Inclui a classe para realizar o roteamento de urls **/
require_once ROOT . '/core/redirector.php';

/**
 * Carrega automaticamente uma classe que é instanciada nas classes que incluem este arquivo
 * @param string $class nome da classe instanciada para realizar o include do arquivo
 */
function autoloadClasses($class) {
   $dir = lcfirst(substr($class, 1)) . "/";
   if(file_exists(ROOT . SERVICES . $dir . $class . '.php') ){
       require_once ( ROOT . SERVICES . $dir . $class . '.php');
   } elseif (file_exists(ROOT . SERVICES . $dir . $class . '.php')) {
      require_once (ROOT . SERVICES . $dir . $class . '.php');
   }elseif (DEBUG) {
         config::getInstance()->debugMessage("Autoloader: class [{$class}] or dir[{$dir}] not found.");
   }
}
spl_autoload_register("autoloadClasses");
