<?php

/**
 * Classe controladora principal, que contém métodos gerais para todos os controllers;
 */
class Controller {

   private $controllerName;

   function __construct() {
      
   }

   public function getControllerName() {
      return $this->controllerName;
   }

   public function setControllerName($controllerName) {
      $this->controllerName = lcfirst($controllerName);
      return $this;
   }
   
   /**
    * Verifica se usuário está logado e se tem acesso ao servico
    * @param string $service Alias do servico Ex.: 'servico_teste'
    * @return boolean
    */
   public function verificaAcesso($service) {
      return true;
   }

   /**
    * Método para ações anteriores à execução da chamada
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    */
   public function init() {
      $redirector = Redirector::getInstance();
      $config = config::getInstance();
      $controllerName = strtolower($this->getControllerName());
      $actionName = $redirector->getAction() ;
      $service = $controllerName . "/" . $actionName;
      if (!in_array($service, $config->getNoAuthorizationRequired())) {
         if (!$this->verificaAcesso($service)) {   
            $vars['message'] = "You are not authorized to make this request.";
            $this->viewError(401, $vars);
            
         }
      }

      $agent = $redirector->getUserAgent();
      $_SESSION['user_agent'] = $agent;
   }

   /**
    * Método padrão para ser realizado ao final de cada requisição
    */
   public function end() {
   }

   /**
    * Verifica se a conexão está sendo feita por HTTPS
    * @return boolean true se o acesso for por HTTPS e false caso contrário
    */
   private function isSecureConnection() {
      return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on');
   }

   protected function view($controller, $viewName, $vars = NULL) {
      $dir = $controller . "/";
      $footer = SERVICES . 'common/mainFooter.php';
      $navBar = SERVICES . 'common/topNavBar.php';
      $viewName = "V" . ucfirst($viewName);
      if (is_array($vars)) {
         extract($vars, EXTR_PREFIX_ALL, '');
      }
      $viewFileName = ROOT . SERVICES . $dir  . $viewName . '.php';
      if(file_exists($viewFileName)){
         require_once $navBar;
         require_once $viewFileName;
         require_once $footer;
         return true;
      }else{
         return false;
      }
   }

   public function viewError($statusCode = 404, $vars = NULL) {
      $this->view('errors', $statusCode, $vars);
      die;
   }

   protected function getParams($key = NULL) {
      $redirector = Redirector::getInstance();
      if (isset($key)) {
         return $redirector->getParam($key);
      } else {
         return $redirector->getParams();
      }
   }

   protected function checkRequiredParams(Array $methodParams) {
      $flag = true;
      $missingParams = array();
      $requestParams = $this->getParams();
      $requestParamsKeys = array_keys($requestParams);
      foreach ($methodParams as $methodParam) {
         if (!in_array($methodParam, $requestParamsKeys) || empty($requestParams["$methodParam"])) {
            $missingParams[] = $methodParam;
         }
      }
      if (count($missingParams) > 0) {
         $message['message'] = "The fields " . implode(',', $missingParams) . " are missing or empty.";
         $this->viewError(422, $message);
         $flag = false;
      }
      return $flag;
   }

   /**
    * Busca Mensagem para respectivos códigos de status HTTP
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $statusCode Código de status HTTP (veja em: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)
    * @return boolean|string Retorna a mensagem ou falso caso não encontre o codigo de status
    */
   public function getMessageStatus($statusCode) {
      /**
       * @var array HTTP response codes and messages
       */
      $messages = array(
         //Informational 1xx
         100 => '100 Continue',
         101 => '101 Switching Protocols',
         //Successful 2xx
         200 => '200 OK',
         201 => '201 Created',
         202 => '202 Accepted',
         203 => '203 Non-Authoritative Information',
         204 => '204 No Content',
         205 => '205 Reset Content',
         206 => '206 Partial Content',
         226 => '226 IM Used',
         //Redirection 3xx
         300 => '300 Multiple Choices',
         301 => '301 Moved Permanently',
         302 => '302 Found',
         303 => '303 See Other',
         304 => '304 Not Modified',
         305 => '305 Use Proxy',
         306 => '306 (Unused)',
         307 => '307 Temporary Redirect',
         //Client Error 4xx
         400 => '400 Bad Request',
         401 => '401 Unauthorized',
         402 => '402 Payment Required',
         403 => '403 Forbidden',
         404 => '404 Not Found',
         405 => '405 Method Not Allowed',
         406 => '406 Not Acceptable',
         407 => '407 Proxy Authentication Required',
         408 => '408 Request Timeout',
         409 => '409 Conflict',
         410 => '410 Gone',
         411 => '411 Length Required',
         412 => '412 Precondition Failed',
         413 => '413 Request Entity Too Large',
         414 => '414 Request-URI Too Long',
         415 => '415 Unsupported Media Type',
         416 => '416 Requested Range Not Satisfiable',
         417 => '417 Expectation Failed',
         418 => '418 I\'m a teapot',
         422 => '422 Unprocessable Entity',
         423 => '423 Locked',
         426 => '426 Upgrade Required',
         428 => '428 Precondition Required',
         429 => '429 Too Many Requests',
         431 => '431 Request Header Fields Too Large',
         432 => '432 Login Error',
         //Server Error 5xx
         500 => '500 Internal Server Error',
         501 => '501 Not Implemented',
         502 => '502 Bad Gateway',
         503 => '503 Service Unavailable',
         504 => '504 Gateway Timeout',
         505 => '505 HTTP Version Not Supported',
         506 => '506 Variant Also Negotiates',
         510 => '510 Not Extended',
         511 => '511 Network Authentication Required'
      );
      if (isset($messages[$statusCode])) {
         return $messages[$statusCode];
      } else {
         return false;
      }
   }

   public function _ajax() {
      $dir = $this->getControllerName();
      $_action = empty($_GET['action']) ? $_POST['action'] : $_GET['action'];
      $file = ROOT . SERVICES . $dir . '/ajax.php';
      if (file_exists($file)) {
         require_once $file;
      } elseif (DEBUG) {
         config::getInstance()->debugMessage("Ajax para {$dir} não encontrado.");
      }
   }

}
