<?php

include_once ROOT . '/services/common/dao.php';

/**
 * Modelo para conexão, utilizando a classe DAO
 */
class Model extends Dao {

   /**
    * Permite que uma consulta seja paginada via GET
    * @var bool 
    */
   private $allowPagination;

   /**
    * Cria uma conexão temporária com o banco utilizando os dados da classe core/config.php
    * A conexão será fechada assim que for terminada uma transação.
    */
   function connect($debug = false) {
      $config = Config::getInstance();
      $dbName = $config->getDBName();
      $dbUser = $config->getDBUsername();
      $dbPw = $config->getDbPassword();
      $dbHost = $config->getDbHost();
      $this->setEncode('utf8');
      $this->setDB($dbName);
      $this->setUser($dbUser);
      $this->setPass($dbPw);
      $this->setHost($dbHost);
      if ($debug) {
         parent::debug('on');
      }
      return parent::connect();
   }

   /**
    * Permite que uma consulta seja paginada via GET
    * @return bool Retorna a permissão para paginação
    */
   public function getAllowPagination() {
      return $this->allowPagination;
   }

   /**
    * Permite que uma consulta seja paginada via GET
    * @param bool $allowPagination
    * @return \Model
    */
   public function setAllowPagination($allowPagination) {
      $this->allowPagination = $allowPagination;
      return $this;
   }

   /**
    * Busca valores no banco de dados e retorna o array da pesquisa.<br/>
    * @param string $campos  <p>
    * Nomes das colunas do banco de dados, devem ser separadas por virgula. Podem 
    * ser utilizados todos os tipos de funções e operadores desejados. 
    * </p>
    * @param string $tabela  <p>
    * Nomes das tabelas do banco de dados, devem ser separadas por virgula ou pode
    * ser utilizado JOIN ou outros operadores
    * </p>
    * @param string $condicao [optional] <p>
    * Condições da consulta. 
    * A expressão <b>GROUP BY</b> pode ser inserida no final da string. 
    * A expressão <b>LIMIT</b> pode ser inserida no final da string. 
    * </p>
    * @param string $ordem [optional] <p>
    * Define a ordenação do retorno da consulta.
    * A expressão <b>LIMIT</b> pode ser inserida no final da string. 
    * </p>
    * @return Array $result
    */
   function select($campos, $tabela, $condicao = NULL, $ordem = NULL) {
      if (empty($ordem)) {
         $condicao = $this->verificaPaginacao($condicao);
      } else {
         $ordem = $this->verificaPaginacao($ordem);
      }
      $executa = parent::select($campos, $tabela, $condicao, $ordem);
      return $executa;
   }

   /**
    * Método para verificar se o limite de resultados não excede o limite especificado em Config::getQuerysMaxLimit;
    * @param string $texto Consulta SQL, com ou sem LIMIT
    * @return string Consulta alterada para realizar paginação
    */
   private function verificaPaginacao($texto) {
      $pageInc = Redirector::getInstance()->getParam('pageInterval');
      $defaultPageInc = Config::getInstance()->getQuerysPaginationIncrement();
      $pageIncrement = !empty($pageInc) && is_numeric($pageInc) ? $pageInc : $defaultPageInc;
      
      if ($pageIncrement === 0 || !$this->getAllowPagination()) {
         return $texto;
      }
      $pageParam = Redirector::getInstance()->getParam('page');
      $page = !empty($pageParam) && is_numeric($pageParam) ? $pageParam : 1; //primeira pagina por default
      $pageLimit['min'] = $pageIncrement * ($page - 1);
      $pageLimit['max'] = $pageIncrement * ($page);
      $limit = implode(',', $pageLimit);
      $matches = NULL;
      $regex = '[(LIMIT)\s(\d+)]';
      if (preg_match($regex, $texto, $matches)) { //verifica se há limite na consulta
         $prevLimit = $matches[2]; //apenas o valor inteiro do limite 
         if ($prevLimit > $pageIncrement) {
            $prevLimit = $limit;
         } else {
            $limit = $prevLimit;
         }
         return preg_replace($regex, "LIMIT {$limit}", $texto);
      } else {
         return $texto . " LIMIT $limit";
      }
   }

}
