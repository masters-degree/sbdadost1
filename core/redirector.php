<?php
class Redirector {
   private static $instance;
   private $url;
   private $controller;
   private $action;
   private $id;
   private $params;

   /**
    * Construtor da classe, realiza a quebra da URL e seta atributos da classe
    */
   private function __construct() {
      $this->setUrl();
      $this->setController();
      $this->setAction();
      $this->setId();
      $this->setParams();
   }
   private function __clone(){
      
   }
   /**
    * Retorna instância única da classe em todo o projeto
    * @return Redirector instancia da classe
    */
   public static function getInstance(){
      if(!isset(self::$instance)){
         self::$instance = new Redirector();
      }
      return self::$instance;
   }
   /**
    * Busca os parâmetros da URL e os separa
    */
   private function setUrl() {
      $url = ($_GET['p']);
      $this->url = explode('%2F', rawurlencode($url)); //'%2F' == '/'
   }
   /**
    * Seta o método controller a partir da URL, default = index
    * Como em www.com.br/controller/acao/identificador
    */
   public function setController() {
      if (!empty($this->url[0])) {
         $controller = $this->url[0];
      } else {
         $controller = "index"; //controller padrao
      }
      $this->controller = ucfirst($controller);
   }
   /**
    * Busca o controller da URL
    * @return string Nome do controller em www.com.br/controller/acao/identificador
    */
   public function getController() {
      return $this->controller;
   }

   /**
    * Seta a ação que está sendo realizada, através da URL. Default = index
    * Como em www.com.br/controller/acao/identificador
    */
   public function setAction() {
      if (!empty($this->url[1])) {
         $action = ($this->url[1]);
      } else {
         $action = "index"; //metodo padrao
      }
      $this->action = $action;
   }
   /**
    * Busca a ação que está sendo realizada
    * @return string Nome da acao  em www.com.br/controller/acao/identificador
    */
   public function getAction() {
      return $this->action;
   }
   /**
    * Busca identificador passado na URL, se existir
    * @return mixed Retorna o identificador em www.com.br/controller/acao/identificador
    */
   public function getId() {
      return $this->id;
   }
   /**
    * Seta o identificador a partir da URL como em www.com.br/controller/acao/identificador
    * @return \Redirector
    */
   public function setId() {
      $this->id = $this->url[2];
      return $this;
   }

   /**
    * Seta parâmetros definidos ou busca os parâmetros da requisição
    * @param array $params Parâmetros para o set
    * @return Redirector Retorna o próprio elemento
    */
   public function setParams(Array $params = NULL) {
      $this->params = empty($params) ? $this->getParamsByRequest() : array_merge($this->getParamsByRequest(), $params);
      return $this;
   }

   /**
    * Busca um parâmetro de acordo com o indice/chave
    * @param string|int $index Chave/Indice do parâmetro
    * @return string Valor do parâmetro
    */
   public function getParam($index) {
      $parametro = NULL;
      if(isset($this->params[$index])){
         $parametro = $this->params[$index];
      }
      return $parametro;
   }

   public function getParams() {
      return $this->params;
   }


   /**
    * Busca parametros de acordo com a requisição que está sendo feita pelo cliente
    * @return array Vetor com parâmetros organizados como chave e valor
    */
   public function getParamsByRequest() {
         $parameters = array_merge($_GET, $_POST);
         unset($parameters['k']); //remove parâmetro para URL rewrite
         return $parameters; //retorna $_GET, $_POST, $_PUT ou $_DELETE
   }
   public function getUserAgent($expected = NULL){
      $userAgent = $_SERVER['HTTP_USER_AGENT'];
      if(empty($userAgent)){
         $agent = false;
      }else{
         if(preg_match("[Dalvik|Android]", $userAgent)){
            $agent = "android";
         }elseif(preg_match("[Darwin|iPhone|iPad|iPod]", $userAgent)){
            $agent = "ios";
         }elseif(preg_match("[Chrome]", $userAgent)){
            $agent = "chrome";
         }else{
            $agent = "unknown";
         }
      }
      if(isset($expected)){
         return $agent == strtolower($expected);
      }else{
         return $agent;
      }
   }
   /**
    * Método que inicia os redirecionamentos da URL para os controllers e métodos
    */
   public function start() {
      $ctrl = new Controller();
      $controllerDir = lcfirst($this->controller) . "/";
      $controllerCamelCase = implode('', array_map('ucfirst', explode("-", $this->controller)));
      $controllerClass = "C" . $controllerCamelCase;
      if (file_exists(SERVICES . $controllerDir . $controllerClass . ".php")) {
         require_once SERVICES . $controllerDir . $controllerClass . ".php";
         $app = new $controllerClass();
         $app->setControllerName($this->controller);
         $metodo = "_" .  $this->action;
         if (isset($metodo)) {
            if (method_exists($app, $metodo)) {
               $app->init(); // método para realizar tratamentos antes do método princial (permissão, autenticação, etc)
               $app->$metodo($this->id);
               $app->end();
            } else {
               config::getInstance()->debugMessage("Método {$this->getAction()} não encontrado.");
               $ctrl->viewError(404);
            }
         }
      } else {
         config::getInstance()->debugMessage("Controller (" . $this->controller . ") não encontrado.");
         $ctrl->viewError(404);
      }
   }
   
   function headersRequired(){
      $noHeaders = config::getInstance()->getNoHeadersRequired();
      $metodo = $this->getAction();
      return !in_array($metodo, $noHeaders);
   }

}