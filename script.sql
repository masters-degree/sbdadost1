-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `all-in` DEFAULT CHARACTER SET utf8 ;
USE `all-in` ;

-- -----------------------------------------------------
-- Table `mydb`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `all-in`.`categories` (
  `category_id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `parent_category_id` INT(11) NULL,
  PRIMARY KEY (`category_id`),
  INDEX `fk_categories_1_idx` (`parent_category_id` ASC),
  CONSTRAINT `fk_categories_1`
    FOREIGN KEY (`parent_category_id`)
    REFERENCES `all-in`.`categories` (`category_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `all-in`.`items` (
  `item_id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `category_id` INT(11) NOT NULL,
  `content` FLOAT(7,2) NULL,
  `packing` VARCHAR(45) NULL,
  `unity` VARCHAR(10) NULL,
  `brand` VARCHAR(45) NULL,
  `image_url` VARCHAR(150) NULL,
  PRIMARY KEY (`item_id`),
  INDEX `fk_items_1_idx` (`category_id` ASC),
  CONSTRAINT `fk_items_1`
    FOREIGN KEY (`category_id`)
    REFERENCES `all-in`.`categories` (`category_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`places`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `all-in`.`places` (
  `place_id` INT(11) NOT NULL AUTO_INCREMENT,
  `corporate_name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`place_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `all-in`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(255) NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`items_prices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `all-in`.`items_prices` (
  `item_price_id` INT(11) NOT NULL AUTO_INCREMENT,
  `item_id` INT(11) NOT NULL,
  `value` FLOAT(5,2) NULL,
  `place_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `collected_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_price_id`),
  INDEX `fk_items_prices_1_idx` (`item_id` ASC),
  INDEX `fk_items_prices_2_idx` (`place_id` ASC),
  INDEX `fk_items_prices_3_idx` (`user_id` ASC),
  CONSTRAINT `fk_items_prices_1`
    FOREIGN KEY (`item_id`)
    REFERENCES `all-in`.`items` (`item_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_items_prices_2`
    FOREIGN KEY (`place_id`)
    REFERENCES `all-in`.`places` (`place_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_items_prices_3`
    FOREIGN KEY (`user_id`)
    REFERENCES `all-in`.`users` (`user_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
