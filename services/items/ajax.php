<?php

// $_action vem do GET ['action'], se for nulo, vem do POST['action']

if ($_action) {
   $controller = new CItems();
   $inputType = INPUT_POST;
   switch ($_action) {
      case 'listarTodos':
         $retorno = $controller->getItemsPrices();
         break;
      case 'listar':
         $itemsIds[] = filter_input($inputType, 'itemId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->getItemsPrices($itemsIds, NULL, NULL, NULL);
         break;
      case 'listarItem':
         $itemId = filter_input($inputType, 'itemId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->getItemById($itemId);
         break;
      case 'removeItem':
         $itemId = filter_input($inputType, 'itemId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->removeItem($itemId);
         break;
      case 'removeItemPrice':
         $itemPriceId = filter_input($inputType, 'itemPriceId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->removeItemPrice($itemPriceId);
         break;
      case 'search':
         $args = array(
            'description' => FILTER_SANITIZE_STRING,
            'categories' => array(
               'filter' => FILTER_VALIDATE_INT,
               'flags' => FILTER_REQUIRE_ARRAY,
               'options' => array('min_range' => 1)
            )
         );
         $input  = filter_input_array($inputType, $args);
         $categories = count($input['categories']) > 0? $input['categories'] : NULL;
         $description = isset($input['description'])? $input['description']: NULL;
         $items = $controller->getItemByKeywords($description, 1, $categories);
         $retorno = $items;
         break;
      case 'salvarItem':
         $itemId = filter_input($inputType, 'itemId', FILTER_SANITIZE_NUMBER_INT);
         $alternativeId = filter_input($inputType, 'alternativeId', FILTER_DEFAULT);
         $categoryId = filter_input($inputType, 'category', FILTER_SANITIZE_NUMBER_INT);
         $description = filter_input($inputType, 'description', FILTER_DEFAULT);
         $brand = filter_input($inputType, 'brand', FILTER_DEFAULT);
         $package = filter_input($inputType, 'package', FILTER_DEFAULT);
         $packageUnits = filter_input($inputType, 'packageUnits', FILTER_DEFAULT);
         $content = filter_input($inputType, 'content', FILTER_SANITIZE_NUMBER_FLOAT);
         $unity = filter_input($inputType, 'unity', FILTER_DEFAULT);
         $imageURL = filter_input($inputType, 'image_url', FILTER_DEFAULT);
         if (!empty($itemId)) {
            $retorno = $controller->updateItem($itemId, $alternativeId, $description, $categoryId, $imageURL, $brand, $content, $package, $unity, $packageUnits);
         } else {
            $retorno = $controller->createNew(NULL, $alternativeId, $description, $categoryId, $imageURL, $content, $unity, $package, $brand, $packageUnits);
         }
         break;
   }
   echo json_encode($retorno);
}