<section>
   <div class="">
      <div class="row">            
         <div class="col m12 s12">
            <div class="row">
               <div class="col s12">
                  <div class="col s12 m9">
                     <h4><?= _("Gerenciamento de Produtos") ?></h4>
                  </div>
                  <div class="fixed-action-btn vertical click-to-toggle" style="bottom: 45px; right: 24px;">
                     <a class="btn-floating btn-large waves-effect waves-light red modal-trigger"  href="#modal-view" onclick="clearModal()"><i class="material-icons">add</i></a>
                  </div>
                  <div class="col s12 m3">
                     <a href="<?= BASE_URL . '/items/export/' ?>" title="<?= _("Exportar todos os produtos") ?>" class="waves-effect waves-light btn" style="top: 20px;" ><i class="material-icons left">file_download</i><?= _("Exportar") ?></a>

                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col s12 m12">
                  <table id="table-items" class="responsive-table hover compact nowrap stripe" data-order='[[ 9, "desc" ], [ 3, "asc" ]]' data-page-length='25'>
                     <thead>
                        <tr>
                           <th ><?= _("Img") ?></th>
                           <th ><?= _("ID") ?></th>
                           <th ><?= _("Cod. Barras") ?></th>
                           <th ><?= _("Descrição") ?></th>
                           <th ><?= _("Categoria") ?></th>
                           <th ><?= _("Fabricante") ?></th>
                           <th ><?= _("Embalagem") ?></th>
                           <th ><?= _("Quantidade") ?></th>
                           <th ><?= _("Unidade") ?></th>
                           <th ><?= _("Unid. p/ embal.") ?></th>
                           <th ><?= _("Ações") ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($_items as $item) { ?>
                           <tr>
                              <td  class="avatar">
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <img src="<?= $item['image_url'] ? $item['image_url'] : BASE_URL . '/assets/images/noimage.jpg' ?>" alt="<?= $item['description'] ?>" class="circle"></td>
                                 </a>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <strong><?= ($item['item_id']) ?></strong>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <strong><?= ($item['alternative_id']) ?></strong>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['description'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['category_name'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['brand'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['package'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['content'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['unity'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)">
                                    <?= $item['package_units'] ?>
                                 </a>
                              </td>
                              <td>
                                 <a title="<?= _("Visualizar") ?>" class="waves-effect waves-light btn-floating modal-trigger" href="#modal-view" onclick="viewItem(<?= $item['item_id'] ?>)"><i class="material-icons">visibility</i></a>
                                 <?php // if ($item['active']) { ?>
                                    <a title="<?= _("Excluir") ?>" class="waves-effect waves-light btn-floating red" onclick="excluiItem('<?= $item['item_id']; ?>', '<?= $item['description']; ?>');">
                                       <i class="material-icons">delete</i>
                                    </a>
                                 <?php // } else { ?>
<!--                                    <a title="<?= _("Ativar") ?>" class="waves-effect waves-light btn-floating green" onclick="ativaItem('<?= $item['item_id']; ?>', '<?= $item['description']; ?>');">
                                       <i class="material-icons">thumb_up</i>
                                    </a>  -->
                                 <?php // } ?>
                              </td>
                           </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <br/>
         </div>
      </div>
   </div>
</section>
<!--- Modal --->
<div id="modal-view" class="modal modal-fixed-footer">
   <div class="modal-content">
      <div class="row">
         <form class="col s12" id='formEdition' name='formEdition'>
            <div class="row">
               <div class="input-field col s6">
                  <img class="responsive-img" id='image' name="image" src="<?= BASE_URL ?>/assets/images/no-image.png">
                  <label for="image"><?= _("Imagem"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="description" name="description" type="text" class="validate">
                  <label for="description"><?= _("Descrição"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="alternativeId" name="alternativeId" type="text" class="validate">
                  <label for="alternativeId"><?= _("Código de barras"); ?></label>
               </div>
               <div class="input-field col s6">
                  <?php
                  include_once ROOT . SERVICES . 'categories/VCategories.components.php';
                  $catComponents = new VCategoriesComponents();
                  echo $catComponents->selectCategories($_categories);
                  ?>
                  <label for="category"><?= _("Categoria"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="image_url" name="image_url" type="text" class="validate" onchange="alteraImagem()">
                  <label for="image_url"><?= _("Imagem URL"); ?></label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <input  id="brand" name="brand" type="text" class="validate">
                  <label for="brand"><?= _("Fabricante"); ?></label>
               </div>
               <div class="input-field col s4">
                  <input id="package" name="package" type="text" class="validate">
                  <label for="package"><?= _("Embalagem"); ?></label>
               </div>
               <div class="input-field col s4">
                  <input id="packageUnits" name="packageUnits" type="text" class="validate">
                  <label for="packageUnits"><?= _("Unidades por embalagem"); ?></label>
               </div>
            </div> 
            <div class="row">
               <div class="input-field col s6">
                  <input  id="content" name="content" type="text" class="validate">
                  <label for="content"><?= _("Quantidade"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="unity" name="unity" type="text" class="validate">
                  <label for="unity"><?= _("Unidade"); ?></label>
               </div>
            </div> 
         </form>
      </div>
   </div>
   <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat "><?= _("Fechar") ?></a>
      <a id="btn-salvar-item" onclick='salvarItem()' class="modal-action modal-close waves-effect waves-green btn "><?= _("Salvar") ?></a>
   </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
         function salvarItem(id) {
            var formData = $("#formEdition").serialize();
            sendAjax("action=salvarItem&itemId=" + id + "&" + formData, function (data) {
               if (data) {
                  Materialize.toast('<?= _('Produto salvo com sucesso') ?>', 5000, 'green', location.reload());
               } else {
                  Materialize.toast('<?= _('Falha ao salvar o produto') ?>', 5000, 'red');
               }
            });
         }

         function excluiItem(id, description) {
            if (confirm("Tem certeza que deseja remover o item " + description + "?")) {
               sendAjax("action=removeItem&itemId=" + id , function (data) {
                  if (data == 1) {
                     Materialize.toast('<?= _('Produto removido com sucesso') ?>', 5000, 'green', location.reload());
                  } else if(data == 1451){
                     Materialize.toast('<?= _('Existem preços associados a este item, exclua-os primeiro e tente novamente.') ?>', 5000, 'red');
                  }else{
                     Materialize.toast('<?= _('Falha ao remover o produto') ?>', 5000, 'red');
                  }
               });
            }
         }

//         function ativaItem(id, description) {
//            if (confirm("Tem certeza que deseja ativar o item " + description + "?")) {
//               sendAjax("action=alterarAtivacaoItem&itemId=" + id + "&active=1", function (data) {
//                  if (data) {
//                     Materialize.toast('<?= _('Produto ativado com sucesso') ?>', 5000);
//                  } else {
//                     Materialize.toast('<?= _('Falha ao ativar o produto') ?>', 5000);
//                  }
//               });
//            }
//
//         }
         /**
          * Envio de requisições AJAX
          * @param {String} data
          * @returns {retorno} */
         function sendAjax(data, callable) {
            $.ajax({
               'data': data
            }).done(callable);
         }
         function alteraImagem(){
            var url = $("#image_url").val();
            var imagem = url ? url : '<?= BASE_URL ?>' + "/assets/images/noimage.jpg";
            $("#image").attr('src', imagem);
         }
         /**
          * 
          * @param {type} id
          * @returns {undefined}
          */
         function viewItem(id) {
            sendAjax("action=listarItem&itemId=" + id, function (data) {
               var item = data[0];
               $("#description").val(item.description);
               $("label[for='description']").addClass('active');
               $("#alternativeId").val(item.alternative_id);
               $("label[for='alternativeId']").addClass('active');
               $("#brand").val(item.brand);
               $("label[for='brand']").addClass('active');
               $("#package").val(item.package);
               $("label[for='package']").addClass('active');
               $("#packageUnits").val(item.package);
               $("label[for='packageUnits']").addClass('active');
               $("#unity").val(item.unity);
               $("label[for='unity']").addClass('active');
               $("#content").val(item.content);
               $("label[for='content']").addClass('active');
               $("#category").val(item.category_id);
               $("#category").material_select();
               var imagem = item.image_url ? item.image_url : '<?= BASE_URL ?>' + "/assets/images/noimage.jpg";
               $("#image").attr('src', imagem);
               $("#image_url").val(imagem);
               $("label[for='image_url']").addClass('active');
               $("#btn-salvar-item").attr('onclick', 'salvarItem(\'' + id + '\')');
            });
         }
         function clearModal() {
            $("#description").val('');
            $("label[for='description']").removeClass('active');
            $("#brand").val('');
            $("label[for='brand']").removeClass('active');
            $("#package").val('');
            $("label[for='package']").removeClass('active');
            $("#alternativeId").val('');
            $("label[for='alternativeId']").removeClass('active');
            $("#unity").val('');
            $("label[for='unity']").removeClass('active');
            $("#packageUnits").val('');
            $("label[for='packageUnits']").removeClass('active');
            $("#content").val('');
            $("label[for='content']").removeClass('active');
            $("#category").val('');
            var imagem = '<?= BASE_URL ?>' + "/assets/images/noimage.jpg";
            $("#image").attr('src', imagem);
            $("#image_url").val('');
            $("label[for='image_url']").removeClass('active');
            $("#btn-salvar-item").attr('onclick', 'salvarItem(\'\')');
         }
         $(document).ready(function () {
            $('.modal-trigger').leanModal();
            $.ajaxSetup({
               'url': baseURL + "/items/ajax?",
               'dataType': 'json',
               'global': 'true',
               'type': 'post'
            });
            $("#table-items").DataTable({select: true});
         });
</script>