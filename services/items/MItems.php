<?php

class MItems extends Model {

   function __construct() {
      
   }

   public function getItemsPrices(array $itemsIds = NULL, $placeId = NULL, $userId = NULL) {
      $where = " 1 = 1";
      $where .= isset($itemsIds) ? " AND itf.item_id IN (" . implode(',', $itemsIds) . ") " : "";
      $where .= isset($placeId) ? " AND itf.place_id = {$placeId} " : "";
      $where .= isset($userId) ? " AND itf.user_id = {$userId} " : "";
      $this->setAllowPagination(true);
      $this->connect();
      $res = $this->select(
            "it.item_id ,
               it.description as 'item',
               it.alternative_id, 
               it.item_id,
               it.image_url,
               itf.value, 
               CONCAT(us.first_name, ' ', us.last_name) as 'user',
               us.user_id, 
               pl.description as 'place',
               pl.place_id ,
               itf.collected_at as 'date',
               cat.description as 'category',
               cat.category_id ,
               catParent.category_id as 'category_parent_id',
               catParent.description as 'category_parent'", "items_prices itf 
               JOIN items it USING (item_id) 
               LEFT JOIN users us USING (user_id)
               LEFT JOIN places pl USING (place_id)
               LEFT JOIN categories cat ON (it.category_id = cat.category_id)
               LEFT JOIN categories catParent ON (cat.parent_category_id = catParent.category_id)
               ", " $where 
            ORDER BY itf.item_id, itf.collected_at DESC");
//       $this->debug("off");;
      $dados = false;
      $itemId = NULL;
      $coletadoEm = NULL;
      $lastColetadoEm = NULL;
      $lastItemId = NULL;
      $itemIndex = -1;
      while ($row = mysqli_fetch_assoc($res)) {
         $itemId = $row['item_id'];
         $coletadoEm = $row['date'];
         if ($itemId != $lastItemId) {
            $itemIndex++;
            $coletadoIndex = -1;
         }
         if ($coletadoEm != $lastColetadoEm) {
            $coletadoIndex++;
         }
         $lastColetadoEm = $row['date'];
         $lastItemId = $row['item_id'];
         $dados[$itemIndex]['description'] = array(
            'name' => $row['item'],
            'id' => $row['item_id'],
            'alternative_id' => $row['alternative_id'],
            'photo' => $row['image_url']
         );
         $dados[$itemIndex]['categories'] = array(
            $row['category'],
            $row['category_parent']
         );
         if ($coletadoIndex == 0) { //traz apenas o último preco (ordem decrescente)
            $dados[$itemIndex]['place'] = $row['place'];
            $dados[$itemIndex]['place_id'] = $row['place_id'];
            $dados[$itemIndex]['user'] = $row['user'];
         }
      }
      return $dados;
   }

   public function edit($itemId, $alternativeId = NULL, $description = NULL, $categoryId = NULL, $imageURL = NULL, $brand = NULL, $content = NULL, $package = NULL, $unity = NULL, $packageUnits = NULL) {
      $this->connect();
      if (isset($description)) {
         $fields[] = " description = " . $this->putQuotes($description);
      }
      if (isset($imageURL)) {
         $fields[] = " image_url = " . $this->putQuotes($imageURL);
      }
      if (isset($brand)) {
         $fields[] = " brand = " . $this->putQuotes($brand);
      }
      if (isset($content)) {
         $fields[] = " content = " . $this->putQuotes($content);
      }
      if (isset($package)) {
         $fields[] = " package = " . $this->putQuotes($package);
      }
      if (isset($packageUnits)) {
         $fields[] = " package_units = " . $this->putQuotes($packageUnits);
      }
      if (isset($unity)) {
         $fields[] = " unity = " . $this->putQuotes($unity);
      }
      if (isset($categoryId)) {
         $fields[] = " category_id = " . $this->putQuotes($categoryId);
      }
      if (isset($alternativeId)) {
         $fields[] = " alternative_id = " . $this->putQuotes($alternativeId);
      }
      $ok = $this->update(
            "items ", implode(',', $fields), "item_id = {$itemId} "
      );
      $this->close();
      return $ok;
   }

   public function create($description, $alternativeId, $categoryId, $imageUrl, $content, $unity, $package, $brand, $packageUnits, $itemId = NULL) {
      $this->connect();
      $this->setEncode('utf8');
      $inserted = $this->insert(
            "items", "item_id, 
                        alternative_id,
                        description,
                        content,
                        unity,
                        package,
                        package_units,
                        brand,
                        category_id,
                        image_url", $this->putQuotes($itemId) . " , " 
            . $this->putQuotes($alternativeId) . " , " . $this->putQuotes($description) .
            ", " . $this->putQuotes($content) . " , " . $this->putQuotes($unity) .
            ", " . $this->putQuotes($package) . " , " . $this->putQuotes($packageUnits) . " , " . $this->putQuotes($brand) .
            ", " . $this->putQuotes($categoryId) . " , " . $this->putQuotes($imageUrl));
      $callback = $inserted ? $this->last_id() : false;
      $this->close();
      return $callback;
   }

   public function getItemPrice($itemId) {
      $this->connect();
      $result = $this->select(
            "itf.item_id,
               itf.collected_at,
               fi.description,
               it.description,
               it.package,
               it.package_units,
               it.content,
               it.unity,
               itf.value,
               itf.place_id", "items_prices itf
             JOIN items it USING (item_id)
               ", "itf.item_id = '{$itemId}''
            ORDER BY collected_at DESC
            ");
      $prices = false;
      while ($row = mysqli_fetch_assoc($result)) {
         $prices[] = $row;
      }
      $this->close();
      return $prices;
   }

   public function createItemPrice($itemId, $value, $userId, $placeId, $collectedAt, $itemPriceId = NULL) {
      $this->connect();
      $this->setEncode('utf8');
      $inserted = $this->insertIgnore(
            "items_prices", "item_price_id, item_id, value, user_id, place_id, collected_at",
            $this->putQuotes($itemPriceId) . " , " . $this->putQuotes($itemId) .
            ", " . $this->putQuotes($value) .
            ", " . $this->putQuotes($userId) . " , " . $this->putQuotes($placeId) .
            ", " . $this->putQuotes($collectedAt)
      );
      $callback = $inserted ? $this->last_id() : false;
      $this->close();
      return $callback;
   }

   /**
    * Busca um produto por palavras chave, que podem ser restringidos pela categoria
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param array $keywords array de strings com as palavras chave
    * @param array $categories [optional] Categorias de produtos, se nada for informado, traz produtos de todas as categorias
    * @return array Vetor com os resultados da consulta
    */
   public function search(array $keywords, array $categories = NULL) {
      $where = "1 = 1 ";
      $where .= isset($categories) ? "AND category_id IN (" . implode(",", $categories) . ") " : "";
      foreach ($keywords as $word) {
         $where .= " AND description LIKE '%{$word}%' ";
      }
      $this->connect();
      $result = $this->select(
            "*", "items", " {$where} 
            ORDER BY description "
      );
      $items = false;
      while ($result && $row = mysqli_fetch_assoc($result)) {
         $items[] = $row;
      }
      $this->close();
      return $items;
   }

   public function remove($itemId) {
      $this->connect();
      $status = $this->delete('items', "item_id = {$itemId}");
      if (!$status) {
         $lastError = $this->getLastError();
         $status = $lastError['id'];
      }
      $this->close();
      return $status;
   }

   public function removePrice($itemPriceId) {
      $this->connect();
      $status = $this->delete('items_prices', "item_price_id = {$itemPriceId}");
      if (!$status) {
         $lastError = $this->getLastError();
         $status = $lastError['id'];
      }
      $this->close();
      return $status;
   }

   public function read($itemId = NULL, $alternativeId = NULL, $description = NULL) {
      $whereItemId = !empty($itemId) ? " AND item_id = {$itemId} " : "";
      $whereAlternativeId = !empty($alternativeId) ? " AND alternative_id = {$alternativeId} " : "";
      $whereDescription = !empty($description) ? " AND description LIKE '%{$description}%' " : "";
      $this->connect();
      $result = $this->select(
            "i.*, c.description as 'category_name'", "items i 
               JOIN categories c USING (category_id)", "1=1 $whereItemId $whereDescription  
            ORDER BY i.description ASC "
      );
      $items = false;
      while ($result && $row = mysqli_fetch_assoc($result)) {
         $items[] = $row;
      }
      $this->close();
      return $items;
   }

}
