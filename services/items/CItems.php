<?php

class CItems extends Controller {

   private $model;

   function __construct() {
      include_once 'MItems.php';
      $this->model = new MItems();
   }

   /**
    * Busca a descrição dos produtos
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @return array Vetor com a descrição dos itens
    */
   public function getItemDescriptions() {
      $items = $this->getAllItems();
      $descriptions = array();
      foreach ($items as $item) {
         $descriptions[] = $item['description'];
      }
      return $descriptions;
   }

   /**
    * Busca todos os itens
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @return array Vetor com todos os itens ou false caso não encontre nenhum resultado
    */
   public function getAllItems() {
      $items = $this->model->read();
      return $items;
   }

   /**
    * Busca um produto pelo seu identificador
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId Identificador de item
    * @return array Vetor com todos os itens ou false caso não encontre nenhum resultado
    */
   public function getItemById($itemId) {
      return $this->model->read($itemId);
   }
   /**
    * Busca um produto pelo seu identificador alternativo
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $alternativeId Identificador alternativo de item
    * @return array Vetor com todos os itens ou false caso não encontre nenhum resultado
    */
   public function getItemByAlternativeId($alternativeId) {
      return $this->model->read(NULL, $alternativeId);
   }

   /**
    * Busca um produto pela sua descrição
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId Identificador de item
    * @param int $active [optional] Se informado, 1 para apenas itens ativos, 0 para itens inativos
    * @return array Vetor com todos os itens ou false caso não encontre nenhum resultado
    */
   public function getItemByDescription($description) {
      return $this->model->read(NULL, $description);
   }

   /**
    * Busca itens de acordo com palavra chave
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param string $keywords Palavras chave separadas por espaços
    * @param array $categories [optional] Identificador de categoria, busca por produtos de apenas uma categoria
    * @return array Vetor com os resultados da busca ou FALSE caso não encontre nenhuma ocorrência
    */
   public function getItemByKeywords($keywords, array $categories = NULL) {
      if(isset($categories)){
         $catController = new CCategories();
         $childrenCategories = $catController->getOnlyChildren($categories);
      }else{
         $childrenCategories = NULL;
      }
      $keywordsArray = explode(" ", $keywords);
      foreach ($keywordsArray as $word) {
         $words[] = strtolower($this->removeAcentos($word));
      }
      return $this->model->search($words, $childrenCategories);
   }

   
   /**
    * Cria um novo item
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId [optional] Identificador do item. Se não informado, utiliza auto-incremento
    * @param int $alternativeId [optional] Identificador alternativo do item. 
    * @param string $description [optional] Descrição (nome) do item
    * @param int $categoryId Identificador da categoria 'filha' do item
    * @param string $imageUrl [optional] URL onde a imagem está hospedada
    * @param float $content [optional] Quantidade do produto na emabalagem
    * @param string $unity [optional] Unidade de medida da quantidade
    * @param string $package [optional] Nome da embalagem (lata, pacote, garrafa, long neck...)
    * @param string $brand [optional] Fabricante do produto
    * @return array Vetor com os resultados da busca ou false caso não encontre nenhuma ocorrência
    */
   public function createNew(
   $itemId = NULL, $alternativeId = NULL, $description = NULL, $categoryId = NULL, $imageUrl = NULL, $content = NULL, $unity = NULL, $package = NULL, $brand = NULL, $packageUnits = 1) {
      return $this->model->create($description, $alternativeId, $categoryId, $imageUrl, $content, $unity, $package, $brand, $packageUnits,  $itemId);
   }
   /**
    * Atualiza um item
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId Identificador do item.
    * @param int $alternativeId [optional] Identificador alternativo do item. 
    * @param string $description [optional] Descrição (nome) do item
    * @param int $categoryId [optional] Identificador da categoria
    * @param string $imageURL [optional] URL onde a imagem está hospedada
    * @param string $brand [optional] Fabricante do produto
    * @param float $content [optional] Quantidade do produto na emabalagem
    * @param string $package [optional] Nome da embalagem (lata, pacote, garrafa, long neck...)
    * @param string $unity [optional] Unidade de medida da quantidade
    * @return type
    */
   public function updateItem($itemId, $alternativeId, $description = NULL, $categoryId = NULL, $imageURL = NULL, $brand = NULL, $content = NULL, $package = NULL, $unity = NULL, $packageUnits = NULL) {
      $status = $this->model->edit($itemId, $alternativeId, $description, $categoryId, $imageURL, $brand, $content, $package, $unity, $packageUnits);
      return $status;
   }
   /**
    * Cria um novo campo associado à um item (por exemplo preço)
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId Identificador do item
    * @param mixed $value Valor do campo
    * @param int $userId Identificador do usuário
    * @param int $placeId Identificador do local (estabelecimento)
    * @param datetime $collectedAt Data da coleta do campo.
    * @param int $itemPriceId [optional] Identificador da associacao campo-item. Se não informado, utiliza auto-incremento
    * @return int Identificador da associacao campo-item ou FALSE em caso de falha.
    */
   public function createNewItemPrice($itemId, $value, $userId, $placeId, $collectedAt, $itemPriceId = NULL) {
      return $this->model->createItemPrice($itemId, $value, $userId, $placeId, $collectedAt, $itemPriceId);
   }
   /**
    * Busca um item, se não encontrar cria um novo.
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param int $itemId [optional] Identificador do item
    * @param string $description [optional] Descrição (nome) do item
    * @param string $categoryId [optional] Identificador da categoria 'filha' do item
    * @param string $imageUrl [optional] URL onde a imagem está hospedada
    * @return int Identificador do item encontrado/criado
    */
   public function getOtherwiseInsert($itemId = NULL, $alternativeId = NULL,  $description = NULL, $categoryId = NULL, $imageUrl = NULL, $content = NULL, $unity = NULL, $package = NULL, $brand = NULL, $packageUnits = NULL) {
      $item = $this->model->read($itemId, $alternativeId, $description);
      if (empty($item)) {
         $id = $this->createNew($itemId, $alternativeId, $description, $categoryId, $imageUrl, $content, $unity, $package, $brand, $packageUnits);
      } else {
         $id = $item[0]['item_id'];
      }
      return $id;
   }
   /**
    * Busca os preços de um item 
    * @param int $itemId Identificador do item
    * @return type
    */
   public function getPricesByItem($itemId) {
      $prices = $this->model->getItemPrice($itemId);
      return $prices;
   }
  
   /**
    * Busca informações da associação de um item com seus campos coletados
    * @param array $itemsIds [optional] Vetor com os identificadores dos itens
    * @param int $placeId [optional] Identificador do local, traz itens apenas de um local
    * @param int $userId [optional] Identificador do usuário, traz itens coletados deste usuário
    * @return array Vetor com os resultados da consulta
    */
   public function getItemsPrices(array $itemsIds = NULL,  $placeId = NULL, $userId = NULL) {
      $items = $this->model->getItemsPrices($itemsIds, $placeId, $userId);
      return $items;
   }
   
   public function removeItem($itemId){
      return $this->model->remove($itemId);
   }
   
   public function removeItemPrice($itemPriceId){
      return $this->model->removePrice($itemPriceId);
   }
   /**
    * Método acessível pelo usuário para buscar por produto(s)
    * @param int $itemId [optional] Identificador do item para detalhar um unico item
    */
   public function _list($itemId = NULL) {
      $this->filter($itemId);
   }
   /**
    * Método view default para os itens
    * @param int $itemId [optional] Identificador do item para detalhar um unico item
    */
   public function _index($itemId = NULL) {
      $this->_manage($itemId);
   }
   /**
    * Método acessível pelo usuário para visualização do gerenciamento de produtos (CRUD)
    * @param int $id Identificador do item
    */
   public function _manage($id = NULL) {
      $categs = new CCategories();
      $vars['categories'] = $categs->getAll();
      if ($id) {
         $vars['item'] = $this->getItemById($id);
      } else {
         $vars['items'] = $this->getAllItems();
      }
      $this->view('items', 'productsManage', $vars);
   }
   /**
    * Método acessível ao usuário para exportar arquivo CSV com todos os itens
    */
   public function _export() {
      header('Content-Type: text/csv; charset=utf-8');
      header('Content-Disposition: attachment; filename=items' . date('dmY_His') . '.csv');
      $output = fopen('php://output', 'w');

      fputcsv($output, array('ID','Código de barras',  'Descricao', 'Fabricante', 'Embalagem', 'Quantidade', 'Unidade','Unidades p/ pacote'));
      $resultados = $this->getAllItems();
      foreach ($resultados as $item) {
         $row = array(ucwords($item['item_id']), $item['alternative_id'], $item['description'], $item['brand'], $item['package'], $item['content'], $item['unity'], $item['package_units']);
         fputcsv($output, $row);
      }
   }
   /**
    * Método para realizar a busca de itens
    * @param type $keyword
    */
   public function search($keyword = NULL) {
      $categs = new CCategories();
      $vars['categorias'] = $categs->getAll();

      if (isset($keyword)) {
         $vars['keyword'] = $keyword;
         $this->view('items', 'productsSearch', $vars);
      }
   }
   /**
    * Método acessível ao usuário para a busca de itens
    * @param string $param Palavra chave da busca
    */
   public function _search($param = NULL) {
      $this->search($param);
   }
   /**
    * @author Matheus Cristian, adaptador por Douglas Haubert <dhaubert.ti@gmail.com>
    * @link http://pt.stackoverflow.com/questions/49645/remover-acentos-de-uma-string-em-php
    * @param string $string texto original
    * @return string Texto sem acentuacao
    */
   public function removeAcentos($string) {
      return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
   }

}
