<?php

class MPlaces extends Model {

   public function read($placeId = NULL, $email = NULL) {
      $wherePlace = isset($placeId) ? " AND place_id = {$placeId}" : "";
      $whereEmail = isset($email) ? " AND email = {$email}" : "";
      $this->connect();
      $result = $this->select(
            "place_id, corporate_name, email, phone, description", "places", "1 = 1 {$wherePlace} {$whereEmail}"
      );
      $users = false;
      while ($row = mysqli_fetch_assoc($result)) {
         $users[] = $row;
      }
      $this->close();
      return $users;
   }

   public function create($placeId = NULL, $email = NULL, $description = NULL, $phone = NULL, $corporateName = NULL) {
      $this->connect();
      $inserted = $this->insert('places', 'place_id, email, description, corporate_name, phone',
            $this->putQuotes($placeId) . "," .
            $this->putQuotes($email) . "," .
            $this->putQuotes($description) . "," .
            $this->putQuotes($corporateName) . ", " .
            $this->putQuotes($phone));
      $status = $inserted ? $this->last_id() : false;
      $this->close();
      return $status;
   }

   public function edit($placeId, $email = NULL, $description = NULL, $phone = NULL, $corporateName = NULL) {
      $this->connect();
      if (isset($email)) {
         $fields[] = " email = " . $this->putQuotes($email);
      }
      if (isset($description)) {
         $fields[] = " description = " . $this->putQuotes($description);
      }
      if (isset($corporateName)) {
         $fields[] = " corporateName = " . $this->putQuotes($corporateName);
      }
      if (isset($phone)) {
         $fields[] = " phone = " . $this->putQuotes($phone);
      }
      $ok = $this->update(
            "places ", implode(',', $fields), "place_id = {$placeId} "
      );
      $this->close();
      return $ok;
   }

   public function remove($placeId) {
      $this->connect();
      $status = $this->delete(
            "places ", "place_id = {$placeId} "
      );
      if (!$status) {
         $lastError = $this->getLastError();
         $status = $lastError['id'];
      }
      $this->close();
      return $status;
   }

}
