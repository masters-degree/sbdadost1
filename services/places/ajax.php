<?php

// $_action vem do GET ['action'], se for nulo, vem do POST['action']

if ($_action) {
   $controller = new CPlaces();
   $inputType = INPUT_POST;
   switch ($_action) {
      case 'listarTodos':
         $retorno = $controller->getAll();
         break;
      case 'listar':
         $placeId = filter_input($inputType, 'placeId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->getPlaceById($placeId);
         break;
      case 'salvar': //salva e edita
         $placeId = filter_input($inputType, 'placeId', FILTER_SANITIZE_NUMBER_INT);
         $corporateName = filter_input($inputType, 'corporateName', FILTER_DEFAULT);
         $description = filter_input($inputType, 'description', FILTER_DEFAULT);
         $email = filter_input($inputType, 'email', FILTER_SANITIZE_EMAIL);
         $phone = filter_input($inputType, 'phone', FILTER_DEFAULT);
         if(empty($placeId)){
            $retorno = $controller->createNew($placeId, $email, $description, $phone, $corporateName);
         }else{
            $retorno = $controller->update($placeId, $email, $description, $phone, $corporateName);
         }
         break;
      case 'remover':
         $placeId = filter_input($inputType, 'placeId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->remove($placeId);
         break;
   }
   echo json_encode($retorno);
}