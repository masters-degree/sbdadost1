<section>
   <div class="">
      <div class="row">            
         <div class="col m12 s12">
            <div class="row">
               <div class="col s12">
                  <div class="col s12 m9">
                     <h4><?= _("Gerenciamento de Estabelecimentos") ?></h4>
                  </div>
                  <div class="fixed-action-btn vertical click-to-toggle" style="bottom: 45px; right: 24px;">
                     <a class="btn-floating btn-large waves-effect waves-light red modal-trigger"  href="#modal-view" onclick="clearModal()"><i class="material-icons">add</i></a>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col s12 m12">
                  <table id="table-items" class="responsive-table hover compact nowrap stripe" data-order='[[ 0, "asc" ],[ 1, "asc"]]' data-page-length='10'>
                     <thead>
                        <tr>
                           <th ><?= _("ID") ?></th>
                           <th ><?= _("Razão Social") ?></th>
                           <th ><?= _("Descrição") ?></th>
                           <th ><?= _("Email") ?></th>
                           <th ><?= _("Telefone") ?></th>
                           <th ><?= _("Ações") ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($_places as $place) { ?>
                           <tr>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)">
                                    <strong><?= ($place['place_id']) ?></strong>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)">
                                    <?= $place['corporateName'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)">
                                    <?= $place['description'] ?>  
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)">
                                    <?= $place['email'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)">
                                    <?= $place['phone'] ?>
                                 </a>
                              </td>
                              <td>
                                 <a title="<?= _("Visualizar") ?>" class="waves-effect waves-light btn-floating modal-trigger" href="#modal-view" onclick="view(<?= $place['place_id'] ?>)"><i class="material-icons">visibility</i></a>
                                 <a title="<?= _("Excluir") ?>" class="waves-effect waves-light btn-floating red" onclick="exclui('<?= $place['place_id']; ?>', '<?= $place['description'] ?>');">
                                    <i class="material-icons">delete</i>
                                 </a>
                              </td>
                           </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <br/>
         </div>
      </div>
   </div>
</section>
<!--- Modal --->
<div id="modal-view" class="modal modal-fixed-footer">
   <div class="modal-content">
      <div class="row">
         <form class="col s12" id='formEdition' name='formEdition'>
            <div class="row">
               <div class="input-field col s6">
                  <input  id="corporateName" name="corporateName" type="text" class="validate">
                  <label for="corporateName"><?= _("Razão Social"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="description" name="description" type="text" class="validate">
                  <label for="description"><?= _("Descrição"); ?></label>
               </div>
            </div> 
            <div class="row">
               <div class="input-field col s4">
                  <input id="email" name="email" type="text" class="validate">
                  <label for="email"><?= _("Email"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="phone" name="phone" type="text" class="validate">
                  <label for="phone"><?= _("Fone"); ?></label>
               </div>
            </div> 
         </form>
      </div>
   </div>
   <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat "><?= _("Fechar") ?></a>
      <a id="btn-salvar" onclick='salvar()' class="modal-action modal-close waves-effect waves-green btn "><?= _("Salvar") ?></a>
   </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
         function salvar(id) {
            var formData = $("#formEdition").serialize();
            sendAjax("action=salvar&placeId=" + id + "&" + formData, function (data) {
               if (data) {
                  Materialize.toast('<?= _('Estabelecimento salvo com sucesso') ?>', 5000, 'green', location.reload());
               } else {
                  Materialize.toast('<?= _('Falha ao salvar o estabelecimento') ?>', 5000, 'red');
               }
            });
         }

         function exclui(id, description) {
            if (confirm("Tem certeza que deseja remover o estabelecimento " + description + "?")) {
               sendAjax("action=remover&placeId=" + id, function (data) {
                  if (data == 1) {
                     Materialize.toast('<?= _('estabelecimento removido com sucesso') ?>', 5000, 'green', location.reload());
                  } else if (data == 1451) {
                     Materialize.toast('<?= _('Existem coletas de preços associadas a este estabelecimento. Exclua-as e tente novamente.') ?>', 5000, 'red');
                  } else {
                     Materialize.toast('<?= _('Falha ao remover o estabelecimento') ?>', 5000, 'red');
                  }
               });
            }
         }
         /**
          * Envio de requisições AJAX
          * @param {String} data
          * @returns {retorno} */
         function sendAjax(data, callable) {
            $.ajax({
               'data': data
            }).done(callable);
         }
         /**
          * 
          * @param {type} id
          * @returns {undefined}
          */
         function view(id) {
            sendAjax("action=listar&placeId=" + id, function (data) {
               var place = data[0];
               $("#description").val(place.description);
               $("#corporateName").val(place.corporate_name);
               $("#email").val(place.email);
               $("#phone").val(place.phone);
               $("label[for='description']").addClass('active');
               $("label[for='corporateName']").addClass('active');
               $("label[for='email']").addClass('active');
               $("label[for='phone']").addClass('active');
               $("#btn-salvar").attr('onclick', 'salvar(\'' + id + '\')');
            });
         }
         function clearModal() {
            $("#description").val('');
            $("label[for='description']").removeClass('active');
            $("#corporateName").val('');
            $("label[for='corporateName']").removeClass('active');
            $("#email").val('');
            $("label[for='email']").removeClass('active');
            $("#phone").val('');
            $("label[for='phone']").removeClass('active');
            $("#btn-salvar").attr('onclick', 'salvar(\'\')');
         }
         $(document).ready(function () {
            $('.modal-trigger').leanModal();
            $.ajaxSetup({
               'url': baseURL + "/places/ajax?",
               'dataType': 'json',
               'global': 'true',
               'type': 'post'
            });
            $("#table-items").DataTable({select: true});
         });
</script>