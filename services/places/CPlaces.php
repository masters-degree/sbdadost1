<?php

class CPlaces extends Controller {

   private $model;

   public function __construct() {
      $this->model = new MPlaces();
   }

   public function createNew($placeId = NULL, $email = NULL, $description = NULL, $phone = NULL, $corporateName = NULL) {
      $status = $this->model->create($placeId, $email, $description, $phone, $corporateName);
      return $status;
   }

   public function update($placeId, $email = NULL, $description = NULL, $phone = NULL, $corporateName = NULL) {
      $status = $this->model->edit($placeId, $email, $description, $phone, $corporateName);
      return $status;
   }

   public function remove($placeId) {
      $status = $this->model->remove($placeId);
      return $status;
   }

   public function getPlaceById($placeId) {
      $user = $this->model->read($placeId);
      return $user;
   }

   public function getAll() {
      $users = $this->model->read();
      return $users;
   }

   public function _index() {
      $vars['places'] = $this->getAll();
      $this->view('places', 'placesManage', $vars);
   }

}
