<?php

/**
 * Classe para manipulacao de log
 */
class Log {

   private $logId = NULL;
   private $dadosPessoaisId = NULL;
   private $tokenCriador = NULL;
   private $servicoId = NULL;
   private $estadoAnterior = NULL;
   private $consulta = NULL;
   private $descricao = NULL;
   private $data = NULL;
   private $ip = NULL;
   private $url = NULL;
   private $operacao = NULL;
   private $model = NULL;
   private $DEBUG = FALSE;
   
   private static $instance;

   /**
    * Banco de dados de log
    */
   const BD = 'all-in';

   public function getLogId() {
      return $this->logId;
   }

   public function getDadosPessoaisId() {
      return $this->dadosPessoaisId;
   }

   public function getTokenCriador() {
      return $this->tokenCriador;
   }

   public function getServicoId() {
      return $this->servicoId;
   }

   public function getEstadoAnterior() {
      return $this->estadoAnterior;
   }

   public function getConsulta() {
      return $this->consulta;
   }

   public function getDescricao() {
      return $this->descricao;
   }

   public function getData() {
      return $this->data;
   }

   public function getIp() {
      return $this->ip;
   }

   public function getUrl() {
      return $this->url;
   }

   public function getOperacao() {
      return $this->operacao;
   }

   public function setLogId($logId) {
      $this->logId = $logId;
   }

   public function setDadosPessoaisId($dadosPessoaisId) {
      $this->dadosPessoaisId = $dadosPessoaisId;
   }

   public function setTokenCriador($tokenCriador) {
      $this->tokenCriador = $tokenCriador;
   }

   public function setServicoId($servicoId) {
      $this->servicoId = $servicoId;
   }

   public function setEstadoAnterior($estadoAnterior) {
      $this->estadoAnterior = $estadoAnterior;
   }

   public function setConsulta($consulta) {
      $this->consulta = $consulta;
   }

   public function setDescricao($descricao) {
      $this->descricao = $descricao;
   }

   public function setData($data) {
      $this->data = $data;
   }

   public function setIp($ip) {
      $this->ip = $ip;
   }

   public function setUrl($url) {
      $this->url = $url;
   }

   public function setOperacao($operacao) {
      $this->operacao = $operacao;
   }

   private function __construct() {
      include_once '/var/www/all-in/services/common/dao.php';
      $this->model = new Dao();
      $this->model->setLOG(false);
      $this->model->connect(true);
      if (isset($_SESSION['debugSistema'])) {
         $this->debug('on');
      }
   }
   
   /**
    * <b>getInstance</b>
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * Retorna uma única instância da classe 
    * <p>
    *    include_once '/var/www/br-html/services/common/Log.php';
         $log = Log::getInstance();
    * </p>
    * @assert () == Log
    */
   public static function getInstance(){
      if(!isset(Log::$instance)){
         Log::$instance = new Log();
      }
      return Log::$instance;
   }
   
   /**
    * <b>debug</b>
    * Ativa ou desativa o debug
    * @param string $status (on|off)
    */
   public function debug($status = '') {
      $this->model->debug($status);

      if (!$status) {
         if ($this->DEBUG == 'on') {
            $this->DEBUG = 'off';
            echo "<pre class='col-md-12 alert alert-warning'>";
            print_r('DEBUG LOG OFF');
            echo "</pre>";
         } else {
            $this->DEBUG = 'on';
            echo "<pre class='col-md-12 alert alert-warning'>";
            print_r('DEBUG LOG ON');
            echo "</pre>";
         }
      } else {
         $this->DEBUG = $status;
         echo "<pre class='col-md-12 alert alert-warning'>";
         print_r("DEBUG LOG " . strtoupper($status));
         echo "</pre>";
      }
   }

   /**
    * * <b>validaParametros</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * @see manipulaLog Doc
    */
   private function validaParametros($operacao = NULL, $dadosPessoaisId = NULL, $tokenCriador = NULL, $servicoId = NULL, $estadoAnterior = NULL, $consulta = NULL, $descricao = NULL, $ip = NULL, $url = NULL, $logId = NULL) {
      /**
       * dados pessoais id
       */
      if (is_null($this->getDadosPessoaisId())) {
         if (is_null($dadosPessoaisId)) {
            $dadosPessoaisId = $_SESSION['usuarioId'];
         }
         $this->setDadosPessoaisId($dadosPessoaisId);
      }
      /**
       * operação
       */
      if (is_null($this->getOperacao()) && is_null($operacao)) {
         $operacao = '';
      }else if (is_null($operacao)) {
         $operacao = $this->getOperacao();
      }
      $this->setOperacao($operacao);
      /**
       * token criador
       */
      if (is_null($this->getTokenCriador())) {
         if (is_null($tokenCriador)) {
            if(isset($_SESSION['tokenCriador'])){
               $tokenCriador = $_SESSION['tokenCriador'];
            }
         }
         $this->setTokenCriador($tokenCriador);
      }
      
      /**
       * servico id
       */      
      if (is_null($this->getServicoId()) && is_null($servicoId)) {
         $servicoId = '';
      }else if (is_null($servicoId)) {
         $servicoId = $this->getServicoId();
      }
      $this->setServicoId($servicoId);      
      /**
       * estado anterior
       */
      if (is_null($this->getEstadoAnterior()) && is_null($estadoAnterior)) {
         $estadoAnterior = '';
      }else if (is_null($estadoAnterior)) {
         $estadoAnterior = $this->getEstadoAnterior();
      }
      $this->setEstadoAnterior($estadoAnterior);   
      /**
       * consulta
       */           
      if (is_null($this->getConsulta()) && is_null($consulta)) {
         $consulta = '';
      }else if (is_null($consulta)) {
         $consulta = $this->getConsulta();
      }
      $this->setConsulta($consulta);
      /**
       * descricao
       */
      if (is_null($this->getDescricao()) && is_null($descricao)) {
         $descricao = '';
      }else if (is_null($descricao)) {
         $descricao = $this->getDescricao();
      }
      $this->setDescricao($descricao);
      /**
       * ip
       */
      if (is_null($this->getIp())) {
         if (is_null($ip)) {
            $ip = "{$_SERVER['REMOTE_ADDR']}";
         }
         $this->setIp($ip);
      }
      /**
       * url
       */
      if (is_null($this->getUrl())) {
         if (is_null($url)) {
            $url = "{$_SERVER['HTTP_REFERER']}";
         }
         $this->setUrl($url);
      }
   }

   /**
    * <b>manipulaLog</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * Manipula as operações de log
     <b>Exemplo:</b>

     <b>CONSULTA A SER REALIZADA:</b><br/>

     $consulta = array('select' => "estacao_id AS 'estacaoId'",
     'from' => "grupos g JOIN dados_pessoais dp USING(dados_pessoais_id)",
     'where' => "dados_pessoais_id = '{$dadosPessoaisId}'");

     <b>INICIALIZAÇÃO DO LOG:<b/><br/>

     include_once '/var/www/puxapreco/services/common/Log.php';
     $log = new Log();

     <b>ESTADO ANTERIOR AUTOMÁTICO:<b/><br/>

     $log->setEstadoAnterior($log->listaEstadoAnterior($consulta['select'], $consulta['from'], $consulta['where']));

     <b>CONSULTA A SER REALIZADA:<b/><br/>

     $result = $this->dao->select($consulta['select'],
     $consulta['from'],
     $consulta['where']);

     <b>PERFORMA AS AÇÕES DE LOG:<b/><br/>

     $log->setConsulta($this->dao->getSQL());
     $log->manipulaLog();
    * @param string $operacao Operação a ser realizadas. Ex.: inserir, editar..
    * @param int $dadosPessoaisId Identificador do operador.
    * @param int $tokenCriador Identificador do operador utilizador de token.
    * @param int $servicoId Identificador do serviço acessado.
    * @param mixed $estadoAnterior Estado da tabela antes das modificações.
    * @param string $consulta Consulta que foi realizada.
    * @param string $descricao Descrição da operação realizada.
    * @param string $ip IP do usuário que está realizando a operação.
    * @param string $url URL que o usuário está acessando.
    * @param int $logId Identificador do log a ser modificado.
    * @return boolean True em caso de sucesso da execucao do log ou false caso contrario
    * @assert ('inserir',NULL,NULL,451,NULL,NULL,'Testes unitarios:inserir',NULL,NULL,NULL) == false
    * @assert ('excluir',NULL,NULL,451,NULL,NULL,'Testes unitarios:excluir',NULL,NULL,NULL) == false
    */
   public function manipulaLog($operacao = NULL, $dadosPessoaisId = NULL, $tokenCriador = NULL, $servicoId = NULL, $estadoAnterior = NULL, $consulta = NULL, $descricao = NULL, $ip = NULL, $url = NULL, $logId = NULL) {

      $this->validaParametros($operacao, $dadosPessoaisId, $tokenCriador, $servicoId, $estadoAnterior, $consulta, $descricao, $ip, $url, $logId);
      $this->formataEstadoAnterior();
      switch ($this->getOperacao()) {
         case 'excluir':
            $retorno = $this->excluiLog();
            break;
         case 'inserir':
         default:
            $retorno = $this->insereLog();
            break;
      }
      return $retorno;
   }

   /**
    * <b>getServico</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * Verifica qual serviço o usuário está acessando em qual sistema
    */
   private function getServico() {
      /**
       * Diferenciacao entre Sistema Irriga e Irrigasystem
       */
//      include_once '/var/www/br-html/services/Login/CLogin.php';
//      $sistema = CLogin::sistema();
//      $servico = $_GET[$sistema['prefixoServicos']];

      /**
       * @todo serviços do irrigasystem não estão padronizados ainda de acordo
       * com a tabela auth_servicos do Sistema Irriga
       */
      if (empty($servico)) {
         return 0;
      }

//      include_once '/var/www/puxapreco/SistemaIrriga/Navigation/CNavigation.php';
//      $navigation = new CNavigation();
//      $servico = $navigation->getServicoByAlias($servico);
//      return $servico->servico_id;
   }

   /**
    * <b>listaEstadoAnterior</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * Usado para listar estado anterior da tupla a ser modificada a partir de
    * da consulta a ser realizada posteriormente
    * @param string $campos colunas da consulta
    * @param string $tabela tabela da consulta
    * @param bool $changeSchema flag caso ocorra alguma alteracao na estrutura da tabela
    * @param string $condicao clausula where da consulta
    * @param string $ordem ordenacao da consulta
    * @param string $bd banco de dados onde a consulta sera realizada
    * @return string|boolean Retorna um texto com o estado anterior da tabela ou do registro
    * @assert ('*','log_operacao',false,NULL,NULL) == false
    */
   public function listaEstadoAnterior($campos, $tabela, $changeSchema = false, $condicao = NULL, $ordem = NULL, $bd = 'IRRIGABR') {
      $this->model->setDB($bd);
      if($changeSchema){ //caso ocorra alguma modificação na estrutura
         $resultado = $this->model->describe($tabela);
      }else{
         $resultado = $this->model->select($campos, $tabela, $condicao, $ordem);
      }      
      if ($resultado && mysql_num_rows($resultado) > 0) {
         while ($linha = mysql_fetch_assoc($resultado)) {
            $estadoAnterior[] = $linha;
         }
         return $estadoAnterior;
      }      
      return FALSE;
   }

   /**
    * <b>formataEstadoAnterior</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * Serializa resultado do estado anterior
    */
   private function formataEstadoAnterior() {
      $estadoAnterior = $this->getEstadoAnterior();
      if(is_array($estadoAnterior)){
         $estadoAnterior = json_encode($estadoAnterior);
      }
      $this->setEstadoAnterior($estadoAnterior);
      return;
   }
   
   private function escapeArray($array) {
      if(!is_array($array)){
        return mysql_real_escape_string($array);
      }
      foreach($array as $key=>$value) {
         if(is_array($value)) { sanitate($value); }
         else { $array[$key] = mysql_real_escape_string($value); }
      }
      return $array;
   }

   /**
    * <b>formataEstadoAnterior</b>
    * @author Tobias Senger <tsenger@sistemairriga.com.br>
    * Realiza a inserção das informações do log no banco de dados
    * @return Retorna o ID do log inserido ou false em caso de falha
    */
   private function insereLog() {
      $tabela = 'log_operacao';

      $colunas = 'usuario_id, token_criador, servico_id, descricao,'
              . 'estado_anterior, consulta, ip, url';

      $valores = "'{$this->getDadosPessoaisId()}', "
              . "'{$this->getTokenCriador()}', "
              . "'{$this->getServicoId()}', "
              . "'" . mysql_real_escape_string($this->getDescricao()) . "', "
              . "'" . $this->escapeArray($this->getEstadoAnterior()) . "', "
              . "'" . $this->escapeArray($this->getConsulta()) . "', "
              . "'{$this->getIp()}', "
              . "'{$this->getUrl()}'";

      $DB = $this->model->getDB();
      $this->model->setLOG(false);
      $this->model->setDB(LOG::BD);
      $insertOK = $this->model->insert($tabela, $colunas, $valores);
      $this->model->setDB($DB);

      if ($insertOK > 0) {
         return $this->model->last_id();
      } else {
         return FALSE;
      }
   }
   
   public function excluiLog() {
      $tabela = 'log_operacao';
      $condicao = "log_id = '{$this->getLogId()}'";
      $this->model->setDB(self::BD);
      $this->model->delete($tabela, $condicao);

      if (mysql_affected_rows() > 0) {
         return TRUE;
      } else {
         return FALSE;
      }
   }

}

