<?php
/**
 * Classe para definição de coluna para persistência com banco de dados
 */
class Coluna {
   /**
    * Nome da coluna que está sendo criada
    * @var string $nomeColuna
    */
   public $nomeColuna;
   /**
    * Tipo da coluna que está sendo criada ('varchar', 'int', 'double')
    * @var string $tipo
    */
   public $tipo;
   /**
    * Tamanho da coluna que está sendo criada
    * @var int $tamanho
    */
   public $tamanho;
   /**
    * Flag de chave primária (true|false)
    * @var bool $isChavePrimaria [optional]
    */
   public $isChavePrimaria;
   /**
    * Flag para identificar se o campo pode ser nulo
    * @var bool $isNull [optional]
    */
   public $isNull;
   /**
    * Flag para identificar se o campo pode ser preenchido por zeros
    * @var bool $zeroFill [optional]
    */
   public $zeroFill;
   /**
    * Valor padrão para o campo
    * @var string|null $valorDefault [optional] 
    */
   public $valorDefault;
   /**
    * Flag para autoincremento da coluna
    * @var bool $isAutoIncrement [optional] 
    */
   public $isAutoIncrement;
   /**
    * Flag para chave estrangeira
    * @var bool $isChaveEstrangeira [optional] 
    */
   public $isChaveEstrangeira;
   /**
    * Cláusula ON UPDATE para chave estrangeira
    * @var string $fkOnUpdate [optional]
    */
   public $fkOnUpdate;
   /**
    * Cláusula ON DELETE para chave estrangeira (RESTRICT, CASCADE, NO ACTION, SET NULL)
    * @var string $fkOnDelete [optional]
    */
   public $fkOnDelete;
   /**
    * Tabela de referência para a chave estrangeira (RESTRICT, CASCADE, NO ACTION, SET NULL)
    * @var string $fkTabela [optional]
    */
   public $fkTabela;
   /**
    * Coluna de referência para a chave estrangeira
    * @var string $fkColuna [optional]
    */
   public $fkColuna;
   /**
    * Banco de dados de referência para a chave estrangeira
    * @var string $fkBanco [optional]
    */
   public $fkBanco;
   
   
   function coluna($nomeColuna = NULL, $tipo = NULL, $tamanho = NULL, $isChavePrimaria = false,
           $isNull = false, $valorDefault = NULL, $isAutoIncrement = false,  $isChaveEstrangeira = false,
           $fkOnUpdate = NULL, $fkOnDelete = false, $fkTabela = false, $fkColuna = false, $fkBanco = false) {
      $this->nomeColuna = $nomeColuna;
      $this->tipo = $tipo;
      $this->tamanho = $tamanho;
      $this->isChavePrimaria = $isChavePrimaria;
      $this->isNull = $isNull;
      $this->valorDefault = $valorDefault;
      $this->isAutoIncrement = $isAutoIncrement;
      $this->isChaveEstrangeira = $isChaveEstrangeira;
      $this->fkOnUpdate = $fkOnUpdate;
      $this->fkOnDelete = $fkOnDelete;
      $this->fkTabela = $fkTabela;
      $this->fkColuna = $fkColuna;
      $this->fkBanco = $fkBanco;
   }

   /**
    * Retorna linha formatada para cláusulas como createTable e alterTable
    * @return string `nomeColuna` tipo(tamanho) [[NOT] NULL] [DEFAULT valorDefault] [AUTO_INCREMENT] 
    */
   public function getColunaFormatada($novoNome = NULL){
            $isNull = $this->isNull? "NULL" : "NOT NULL";
            $zeroFill = $this->zeroFill? "ZEROFILL" : "";
            $valorDefault = $this->valorDefault? "DEFAULT '{$this->valorDefault}'" : "";
            $tamanho = $this->tamanho ? '('. $this->tamanho.')' : "" ;
            $nomeColuna = $this->nomeColuna;
            $tipo = $this->tipo;
            $isAutoIncrement = "";
            if(!empty($this->isAutoIncrement)){              
               if($this->isAutoIncrement && $this->isChavePrimaria){
                  $isAutoIncrement = "AUTO_INCREMENT";
               }
               else{
                  echo "Only the primary key can be auto-increment.";
                  return NULL;
               }
            }
            return "`$nomeColuna` $novoNome $tipo $tamanho $isNull $zeroFill $valorDefault $isAutoIncrement ";
   }
   
   public function getChaveEstrangeira(){
         if($this->isChaveEstrangeira){
            return
                    ", FOREIGN KEY (`".$this->nomeColuna."`) "
                    . "REFERENCES `".$this->fkBanco.".".$this->fkTabela."` (`".$this->fkColuna."`) "
                    . "ON DELETE ".$this->fkOnDelete." ON UPDATE ".$this->fkOnUpdate;
         }
         else{
            return NULL;
         }
   }

}

