<?php


/**
 * 
 * Data Acess Object
 */
class Dao {

    public $conecta;
    protected $DEBUG;
    protected $QUEBRAI = "<div style='width: 100%;' class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><pre>";
    protected $QUEBRAF = "</pre></div>";
    protected $DB = "all-in";
    protected $HOST = "localhost";
    protected $USER = "root";
    protected $PASS = "root";
    protected $ENCODE = "utf8";
    protected $SQL = "";
    private $LOG = false;
    private $lastError = NULL;

    /**
     
     * Verifica e modifica as variaveis de quebra de linha do DEBUG caso esteja 
     * rodando em um terminal as variáveis de .
     */
    function dao() {
        include_once 'coluna.php';
        if (php_sapi_name() == 'cli') {
            $this->QUEBRAI = "\n";
            $this->QUEBRAF = "\n";
            $this->setLOG(false);
        }
    }

    /**
     
     * Liga ou desliga o modo debug.
     * @param string $status [opcional]
     * <p>
     * String para iniciar ou parar o modo debug.
     * </p>
     * <p>
     * Utilizando debug no modo automático: para ligar o modo debug basta chamar 
     * o método e para desligar basta chamar o método novamente. 
     * </p>
     * <pre><code>
     * $dao = new Dao();
     * $dao->debug();
     * $dao->selecionar("campo1, campo2","tabela","campo3 = '1'","campo1");
     * $dao->debug();
     * </code></pre>
     * <p>
     * Para forçar a parada ou forçar o inicio do modo debug usamos:
     * <pre><code>
     * $dao->debug('on');
     * // Força o inicio do modo debug
     * 
     * $dao->debug('off');
     * // Força a parada do modo debug
     * </code></pre>
     * </p>
     */
    function debug($st = "") {
        if (!$st) {
            if ($this->DEBUG == "on") {
                $this->DEBUG = "off";
                echo "{$this->QUEBRAI}DEBUG OFF{$this->QUEBRAF}";
            } else {
                $this->DEBUG = "on";
                echo "{$this->QUEBRAI}DEBUG ON{$this->QUEBRAF}";
            }
        } else {
            $this->DEBUG = $st;
            echo "{$this->QUEBRAI}DEBUG {$st}{$this->QUEBRAF}";
        }
    }

    function getLOG() {
        return $this->LOG;
    }

    function setLOG($LOG) {
        $this->LOG = $LOG;
        return $this;
    }

    /**
     
     * Seta o nome do banco da conexão.
     * @param String $DB
     * <pre><code>
     * $dao = new Dao();
     * $dao->setDB('IRRIGABR');
     * </code></pre>
     */
    protected function setDB($DB) {
        $this->DB = $DB;
        if (gettype($this->conecta) == "resource") {
            mysqli_select_db($this->conecta, $this->DB);
        }
    }

    /**
     
     * Retorna a última sql executada pelo objeto.
     * <pre><code>
     * $dao = new Dao();
     * $dao->select("*","tabela","1=1");
     * $dao->getSQL();
     * </code></pre>
     */
    public function getSQL() {
        return $this->SQL;
    }

    /**
     
     * Retorna o nome do banco da conexão.
     * @param String $u
     * <pre><code>
     * $dao = new Dao();
     * $db = $dao->getDB();
     * </code></pre>
     */
    protected function getDB() {
        return $this->DB;
    }

    /**
     
     * Seta o nome de usuário da conexão com o banco de dados.
     * @param String $u
     * <pre><code>
     * $dao = new Dao();
     * $dao->setUser('root');
     * </code></pre>
     */
    protected function setUser($u) {
        $this->USER = $u;
    }

    /**
     
     * Seta senha da conexão com o banco de dados.
     * @param String $p
     * <pre><code>
     * $dao = new Dao();
     * $dao->setPass('password');
     * </code></pre>
     */
    protected function setPass($p) {
        $this->PASS = $p;
    }

    /**
     
     * Seta o host onde se encontra o banco de dados.
     * @param String $h
     * <pre><code>
     * $dao = new Dao();
     * $dao->setHost('localhost');
     * </code></pre>
     */
    protected function setHost($h) {
        $this->HOST = $h;
    }

    /**
     
     * Seta encode utilizado na comunicação com o banco de dados.
     * @param String $h
     * <pre><code>
     * $dao = new Dao();
     * $dao->setEncode('utf8');
     * </code></pre>
     */
    protected function setEncode($e = "") {
        if ($e) {
            $this->ENCODE = $e;
        }
        if($this->ENCODE && $this->conecta){
            mysqli_set_charset($this->conecta, $this->ENCODE);
        }
    }

    /**
     
     * Retorna o usuário configurado para cnexão com o banco de dados.
     * <pre><code>
     * $dao = new Dao();
     * $user = $dao->getUser();
     * </code></pre>
     */
    protected function getUser() {
        return $this->USER;
    }

    /**
     
     * Retorna o usuário configurado para cnexão com o banco de dados.
     * <pre><code>
     * $dao = new Dao();
     * $user = $dao->getUser();
     * </code></pre>
     */
    protected function getPass() {
        return $this->PASS;
    }

    /**
     
     * Retorna o host configurado para cnexão com o banco de dados.
     * <pre><code>
     * $dao = new Dao();
     * $host = $dao->getHost();
     * </code></pre>
     */
    protected function getHost() {
        return $this->HOST;
    }

    /**
     
     * Retorna o encode configurado para cnexão com o banco de dados.
     * <pre><code>
     * $dao = new Dao();
     * $encode = $dao->getEncode();
     * </code></pre>
     */
    protected function getEncode() {
        return $this->ENCODE;
    }

    /**
     
     * Cria uma conexão temporária com o banco.
     * A conexão será fechada assim que for terminada uma transação.
     * <pre><code>
     * $dao = new Dao();
     * $encode = $dao->conecta();
     * </code></pre>
     */
   protected function connect() {
      $this->conecta = mysqli_connect($this->HOST, $this->USER, $this->PASS, $this->DB) or print("<div style='border: 2px dashed #E33;'>ERRO AO CONECTAR!{$this->QUEBRAI}" . mysqli_error() . "{$this->QUEBRAF}</div>");
        mysqli_select_db($this->conecta, $this->DB);
        $this->showDebugError($this->DEBUG);
        if(isset($this->ENCODE)){
           mysqli_set_charset($this->conecta,$this->ENCODE);
        }
        return $this->conecta;
    }

    /**
     * Fecha a conexão com o banco, deve ser utilizada quando aberta uma conexão persistente
     */
    protected function close() {
        if (gettype($this->conecta) == "resource") {
            mysqli_close($this->conecta);
        }
    }

    /**
     *  Inicia uma transação (START TRANSACTION)
     * @return bool Retorna uma flag para o inicio da transação
     */
    protected function start() {
        $this->SQL = "START TRANSACTION";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Confirma uma transação (COMMIT)
     * @return bool Retorna uma flag para a confirmação da transação
     */
    protected function commit() {
        $this->SQL = "COMMIT";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Cancela uma transação (ROLLBACK), 
     * desfazendo todas as querys realizadas dentro do período ativo da transação
     * @return bool Retorna uma flag para o cancelamento da transação
     */
    protected function rollback() {
        $this->SQL = "ROLLBACK";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Segura a tabela informada para que apenas uma pessoa escreva nela por vez
     * @param string $condicao
     * @param string $rw [optional] READ ou WRITE, caso nenhum valor seja passado, rw assume bloqueio contra escrita
     * @return bool Retorna uma flag para confirmação do bloqueio de transação
     */
    protected function lock($condicao, $rw = "") {
        if (!$rw) {
            $rw = "WRITE";
        }
        $this->SQL = "LOCK TABLE $condicao $rw";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Desbloqueia a tabela para que outra pessoa tenha acesso a ela
     * @return bool Retorna uma flag para confirmação do desbloqueio de tabelas
     */
    protected function unlock() {
        $this->SQL = "UNLOCK TABLES";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     
     * Insere no banco de dados<br/>
     * @param type $tabela <p>
     * Nome da tabela cujo dado deve ser inserido
     * </p>
     * @param string $campos  <p>
     * Nomes das colunas do banco de dados, devem ser separadas por virgula. 
     * </p>
     * @param string $dados <p>
     * Valores das colunas do banco de dados, devem ser separadas por virgula e 
     * obedecer a mesma orde das colunas.
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->inserir("tabela","campo1, campo2, campo3","'11.20', '2014-10-10', '15:30:00'");
     * </code></pre>
     * @return resource $executa
     */
    protected function insert($tabela, $campos, $dados) {
        $camposValues = "";
        if ($campos) {
            $camposValues = "($campos)";
        }
        $this->SQL = "INSERT INTO $tabela $camposValues values ($dados)";
        $logId = $this->log($this->SQL);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     
     * Insere no banco de dados, caso o dado já exista ele não será inserido porém 
     * não será mostrada mensagem de erro. Em multiplos inserts, os que já existem
     * não são inseridos e o que não existem são.<br/>
     * @param type $tabela <p>
     * Nome da tabela cujo dado deve ser inserido
     * </p>
     * @param string $campos  <p>
     * Nomes das colunas do banco de dados, devem ser separadas por virgula. 
     * </p>
     * @param string $dados <p>
     * Valores das colunas do banco de dados, devem ser separadas por virgula e 
     * obedecer a mesma orde das colunas.
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->insertIgnore("tabela","campo1, campo2, campo3","'11.20', '2014-10-10', '15:30:00'");
     * </code></pre>
     * @return resource $executa
     */
    protected function insertIgnore($tabela, $campos, $dados) {
        $camposValues = "";
        if ($campos) {
            $camposValues = "($campos)";
        }
        $this->SQL = "INSERT IGNORE INTO $tabela $camposValues values ($dados)";
        $logId = $this->log($this->SQL);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     
     * Insere no banco de dados<br/>
     * @param type $tabela <p>
     * Nome da tabela cujo dado deve ser inserido
     * </p>
     * @param string $campos  <p>
     * Nomes das colunas do banco de dados, devem ser separadas por virgula. 
     * </p>
     * @param string $dados <p>
     * Valores com todas as colunas do banco de dados, devem ser separadas por virgula e 
     * obedecer a mesma ordem das colunas ex: \n
     * (col1_val1, col2_val2, col3_val3), 
     * (col1_val4, col2_val5, col3_val6), 
     * (col1_val7, col2_val8, col3_val9)
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->inserir("tabela","campo1, campo2, campo3","'11.20', '2014-10-10', '15:30:00'");
     * </code></pre>
     * @return resource $executa
     */
    protected function insertBatch($tabela, $campos, $dados) {
        $camposValues = "";
        if ($campos) {
            $camposValues = "($campos)";
        }
        $this->SQL = "INSERT IGNORE INTO $tabela $camposValues values $dados";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    protected function insertSelect($campos, $tabelaSelect, $tabelaInsert, $condicao) {
        $res = $this->select($campos, $tabelaSelect, $condicao);
        $insertValues = "";
        $counter = 0;
        $dataCounter = 0;
        $numRows = mysqli_num_rows($res);
        $existeResultado = ($res && $numRows > 0);
        if ($existeResultado) {
            while ($l = mysqli_fetch_assoc($res)) {
                $insertValues[] = "('" . implode("','", $l) . "')";
                $counter++;
                $dataCounter++;
                if ($counter >= 5500 || $dataCounter >= $numRows) { //insere de 5500 em 5500 resultados
                    $insertDados = implode(',', $insertValues);
                    $this->insertBatch($tabelaInsert, $campos, $insertDados);
                    $insertValues = array();
                    $counter = 0;
                }
            }
        }
        return $dataCounter;
    }

    /**
     * Retorna o último ID inserido na conexão mysqli deste objeto.
     * @return int ID de mysqli_insert_id
     */
    protected function last_id() {
        return mysqli_insert_id($this->conecta);
    }
    
    /**
     * Adiciona aspas se o valor nao for nulo, ou a palavra NULL, caso seja nulo.
     * @param mixed $value Valor em inserts, replaces ou updates
     * @return mixed Valor modificado
     */
    public function putQuotes($value){
      if($value == NULL){
         $modifiedValue = 'NULL';
      }else{
         $modifiedValue = "'$value'";
      }
      return $modifiedValue;
    }
    
    /**
     
     * Substitui no banco de dados;<br/>
     * Obs.: se nao for passado o ID, replace insere com o novo ID<br/>
     * @param type $tabela <p>
     * Nome da tabela cujo dado deve ser substituído.
     * </p>
     * @param string $campos  <p>
     * Nomes das colunas do banco de dados, devem ser separadas por virgula. 
     * </p>
     * @param string $dados <p>
     * Valores das colunas do banco de dados, devem ser separadas por virgula e 
     * obedecer a mesma ordem das colunas.
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->replace("tabela","campo1, campo2, campo3","'11.20', '2014-10-10', '15:30:00'");
     * </code></pre>
     * @return resource $executa
     */
    protected function replace($tabela, $campos, $dados) {
        $camposValues = "";
        if ($campos) {
            $camposValues = "($campos)";
        }
        $this->SQL = "REPLACE INTO $tabela $camposValues values ($dados)";
        $logId = $this->log($this->SQL, $tabela, false, $this->getUniqueCondition($tabela, $campos, $dados));
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     
     * Altera valores no banco de dados e retorna se o status da operação<br/>
     * @param string $tabela <p>
     * Nome da tabela cujo dado deve ser atualizado
     * </p>
     * @param string $campos_dados  <p>
     * Nomes das colunas do banco de dados e seus respectivos novos valores, devem 
     * ser separadas por virgula. Podem ser utilizados todos os tipos de funções 
     * e operadores desejados. 
     * </p>
     * @param string $condicao <p>
     * Condição para atualização
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->update("tabela","campo1 = '10.20', campo2='2014-10-10'","campo3 = 'vendido'");
     * </code></pre>
     * @return int mysqli_affected_rows
     */
    protected function update($tabela, $campos_dados, $condicao) {
        if (!empty($condicao)) {
            $this->SQL = "UPDATE $tabela SET $campos_dados WHERE $condicao";
        } else {
            $this->SQL = "UPDATE $tabela SET $campos_dados";
        }
        $logId = $this->log($this->SQL, $tabela, false, $condicao);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return mysqli_affected_rows($this->conecta);
    }

    /**
     
     * Remove valores do banco de dados e retorna se o status da operação<br/>
     * @param string $tabela <p>
     * Nome da tabela cujo dado deve ser excluído
     * </p>
     * @param string $condicao <p>
     * Condição para exclusão
     * </p>
     * <br/>
     * Exemplo:
     * <pre><code>
     * $dao = new Dao();
     * $dao->delete("tabela","campo12 IS NULL");
     * </code></pre>
     * @return int Retorna o número de linhas afetadas pelo delete ou NULL em caso de falha
     */
    protected function delete($tabela, $condicao) {
        if ($condicao != '') {
            $this->SQL = "DELETE FROM $tabela WHERE $condicao";
        } else {
            $this->SQL = "DELETE FROM $tabela";
        }
        $logId = $this->log($this->SQL, $tabela, false, $condicao);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $return = $executa ? mysqli_affected_rows($this->conecta) : NULL;
        $this->showDebugError($this->DEBUG);
        return $return;
    }

    /**
     
     * Busca valores no banco de dados e retorna o resultset da pesquisa.<br/>
     * @param string $campos  <p>
     * Nomes das colunas do banco de dados, devem ser separadas por virgula. Podem 
     * ser utilizados todos os tipos de funções e operadores desejados. 
     * </p>
     * @param string $tabela  <p>
     * Nomes das tabelas do banco de dados, devem ser separadas por virgula ou pode
     * ser utilizado JOIN ou outros operadores
     * </p>
     * @param string $condicao [optional] <p>
     * Condições da consulta. A expressão <b>GROUP BY</b> pode ser inserida no 
     * final da string. 
     * </p>
     * @param string $ordem [optional] <p>
     * Define a ordenação do retorno da consulta
     * </p>
     * <p>
     * Exemplo de utilização
     * </p>
     * <pre><code>
     * $dao = new Dao();
     * $dao->select("campo1, campo2","tabela","campo3 = '1' ORDER BY campo1","campo1");
     * </code></pre>
     * 
     * @return resource $executa
     */
    protected function select($campos, $tabela, $condicao = NULL, $ordem = NULL) {
        if (isset($ordem)) {
            $ordem = " ORDER BY {$ordem}";
        }
        if ($condicao != '') {
            $this->SQL = "SELECT $campos FROM $tabela WHERE $condicao $ordem";
        } elseif (empty($ordem) || is_null($ordem)) {
            $this->SQL = "SELECT $campos FROM $tabela";
        } else {
            $this->SQL = "SELECT $campos FROM $tabela $ordem";
        }
        $logId = $this->log($this->SQL);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     
     * Cria uma nova tabela
     * @param string $tabela
     * <p> Tabela a ser alterada </p>
     * @param Coluna[] $colunas
     * <p>Array do tipo Coluna </p>
     * <pre><code>
     *   include 'coluna.php';
      $dao = new Dao();
      $tabela[0] = new Coluna();
      $tabela[0]->nomeColuna = 'tabela_id';
      $tabela[0]->tipo = 'int';
      $tabela[0]->tamanho = 11;
      $tabela[0]->isChavePrimaria = true;
      $tabela[0]->isNull = false;
      $tabela[0]->valorDefault = false;
      $tabela[0]->isAutoIncrement = true;
      $tabela[0]->isChaveEstrangeira = true;
      $tabela[0]->fkBanco = 'MYDB';
      $tabela[0]->fkColuna = 'Estacao_ID';
      $tabela[0]->fkTabela = 'estacoes';
      $tabela[0]->fkOnUpdate = 'RESTRICT';
      $tabela[0]->fkOnDelete = 'RESTRICT';

      $dao->createTable('estacoes_ex', $tabela);
     * </code></pre>
     * @return resource $executa 
     */
    protected function createTable($tabela, $colunas) {
        foreach ($colunas as $coluna) {
            /* Concatena no formato da coluna */
            $colunasFormatadas[] = $coluna->getColunaFormatada();
            if ($coluna->isChavePrimaria) {
                $chavesPrimarias[] = $coluna->nomeColuna;
            }
            if ($coluna->isChaveEstrangeira) {
                $chavesEstrangeiras[] = $coluna->getChaveEstrangeira();
            }
        }
        if (count($chavesPrimarias)) {
            $primary_key = ", PRIMARY KEY (" . implode(', ', $chavesPrimarias) . ")";
        }
        //os nomes das chaves estrangeiras são criados pelo mysqli padrão <tabela>_ibfk_<index>
        if (count($chavesEstrangeiras)) {
            $foreign_key = " " . implode(', ', $chavesEstrangeiras);
        }
        /* Adiciona constraint para chaves primárias */
        $this->SQL = "CREATE TABLE IF NOT EXISTS
                           `$tabela`
                           (
                           " . implode(', ', $colunasFormatadas) . "
                              $primary_key
                              $foreign_key
                          )
                  ";
        $logId = $this->log($this->SQL);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        //retorna true em caso da tabela ter sido criada. Se já existir ou falhar, retorna false
        $warningCount = mysqli_query($this->conecta, "SHOW WARNINGS");
        $warningC = mysqli_fetch_assoc($warningCount);
        $flagNaoExiste = strpos($warningC['Message'], 'already exists') === false;
        $flagExecutouQuery = $executa != false;
        $retorno = $flagNaoExiste && $flagExecutouQuery;
        return $retorno;
    }

    /**
     * Comando SQL AlterTable, para alterar as propriedades de uma tabela
     * @param string $tabela
     * <p> Tabela a ser alterada </p>
     * @param Coluna $coluna 
     * <p>Objeto coluna com os atributos necessários para manipulação de coluna</p>
     * @param string|null $nomeNovaColuna 
     * <p>Nome para renomeação da coluna</p>
     * @param string|null $extra
     * Características extra para a alteração da coluna
     * <pre><code>
     * $dao = new Dao();
     *  $coluna = new Coluna();
     *  $coluna->isNull = true;
     *  $coluna->nomeColuna = 'nome';
     *  $coluna->tipo = 'varchar';
     *  $coluna->tamanho = 50;
     *  $dao->alterTable('estacoes', $coluna, 'nome_modificado',  'DEFAULT "unknown"');
     * </code></pre>
     * @return resource $executa 
     */
    protected function alterTable($tabela, $coluna, $nomeNovaColuna = NULL, $extra = NULL) {
        $change_modify = $coluna->nomeColuna != $nomeNovaColuna ? "CHANGE" : "MODIFY";
        $colunaFormatada = $coluna->getColunaFormatada($nomeNovaColuna);
        $this->SQL = "ALTER TABLE 
                           `$tabela`
                  $change_modify COLUMN
                           $colunaFormatada $extra
                  ";
        $logId = $this->log($this->SQL, $tabela, true);
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->revertLog($logId, $executa);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Descreve o schema de uma tabela
     * @param string $tabela Nome da tabela
     * @return type
     */
    public function describe($tabela) {
        $this->SQL = "SHOW FULL COLUMNS FROM $tabela";
        $executa = mysqli_query($this->conecta, $this->SQL);
        $this->showDebugError($this->DEBUG);
        return $executa;
    }

    /**
     * Busca último erro da conexão ativa
     * @return string String formatada com o erro
     */
    public function getError() {
        return "{$this->QUEBRAI}" . mysqli_error($this->conecta) . "{$this->QUEBRAF}";
    }

    /**
     * Exibe mensagem de debug caso este esteja ligado
     * @param string $debug 'on' | 'off'
     */
    private function showDebugError($debug) {
        $this->lastError['id'] =  mysqli_errno($this->conecta);
        $this->lastError['message'] =  mysqli_error($this->conecta);
        if ($debug == 'on' && $this->getDB() != 'pp-log') {
            echo "{$this->QUEBRAI}SQL:\n{$this->SQL}{$this->QUEBRAF}";
            if (!empty($this->lastError['id'])) {
                echo "{$this->QUEBRAI}ERRO:\n[{$this->lastError['id']}::{$this->lastError['message']}]{$this->QUEBRAF}";
            }
        }
    }
    
    public function getLastError(){
       return $this->lastError;
    }
    /**
     * Busca o nome das colunas que contém index do tipo UNIQUE na tabela
     * @param string $tabela Nome da tabela na base de dados
     * @return array|null Retorna um array de strings com o nome das colunas do índice ou NULL caso não encontre
     */
    private function getUniqueIndexes($tabela) {
        $sql = "SHOW INDEXES FROM {$tabela} WHERE Key_name <> 'PRIMARY' AND Non_unique = 0";
        $resTableIndexes = mysqli_query($this->conecta, $sql);
        $uniqueIndexes = array();
        while ($resTableIndexes && $l = mysqli_fetch_assoc($resTableIndexes)) {
            $uniqueIndexes[] = $l['Column_name'];
        }
        return $uniqueIndexes;
    }

    /**
     * Busca uma condição para consultar uma tabela através de seus índices,
     * para consultas como REPLACE onde não há condição
     * <p>Ex.: 
     * $tabela = "visit_schedule";
     * $campos = "visit_id,
      pivo_id,
      purposes,
      report,
      estadio_fenologico,
      horimetro,
      populacao,
      altura_planta,
      invasora,
      umidade,
      ifoliar,
      ps_id,
      status,
      obs";
     * $dados = "30775,
      6273,
      '#9#9',
      'TESTE',
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      18151,
      1,
      NULL";
     * $sql = "REPLACE INTO $tabela $campos values ($dados)";
     * $condicao = $this->getUniqueCondition($tabela, $campos, $dados);
      echo $condicao; // $condicao = "visit_id='30775',pivo_id='6273',ps_id='18151'"
     * 
     * </p>
     * @param string $tabela Tabela com indice UNIQUE
     * @param string $campos Campos da consulta
     * @param string $valores Valores da consulta
     * @return string condiçao pronta para usar no WHERE com indices e valores de uma consulta como
     */
    private function getUniqueCondition($tabela, $campos, $valores) {
        $explodedCampos = array_map('trim', explode(',', $campos));
        $explodedValores = array_map('trim', explode(',', $valores));
        $uniqueIndexes = $this->getUniqueIndexes($tabela);
        $condicoes = array();
        foreach ($uniqueIndexes as $index) {
            $indexCampos = false;
            $indexCampos = array_search($index, $explodedCampos);
            if (is_numeric($indexCampos)) {
                $valor = $explodedValores[$indexCampos];
                $condicoes[] = "{$index}='{$valor}'";
            }
        }
        if (count($condicoes) > 0) {
            $condicao = implode(' AND ', $condicoes);
            return $condicao;
        } else {
            return NULL;
        }
    }

    /**
     * Realiza auditoria na classe para armazenada quem, onde e quando foram executadas as consultas
     * e também o seu estado anterior opcionalmente.
     * @param string $sql consulta executada
     * @param string $tabela [optional] nome da tabela para armazena o estado anterior.
     * @param bool $changeSchema [optional] Flag informando se houve alteração na estrutura da tabela ou BD, default = false
     * @param string $condicao [optional] condicao da consulta para salvar o estado anterior
     * @param string $ordem [optional] ordem da consulta
     * @return boolean|int Retorna o ID do log inserido ou false em caso de falha
     */
    private function log($sql, $tabela = NULL, $changeSchema = false, $condicao = NULL, $ordem = NULL) {
        if ($this->getLOG() && $this->getDB() != 'pp-log') {
            include_once '/var/www/puxapreco/services/common/log.php';
            $database = $this->getDB();
            $log = Log::getInstance();
            $log->setConsulta($sql);
            $log->setDescricao("dao.php");
            if (isset($tabela)) {
                $estadoAnterior = $log->listaEstadoAnterior('*', $tabela, $changeSchema, $condicao, $ordem, $database);
                $log->setEstadoAnterior($estadoAnterior);
            }
            $logId = $log->manipulaLog('inserir');
            return $logId;
        }
        return false;
    }

    /**
     * Reverte Log se a consulta armazenada não foi bem sucedida
     * @param int $logId Identificador do log inserido
     * @param bool $success flag do sucesso da consulta
     * @return boolean Retorna sucesso da execução da exclusao
     */
    private function revertLog($logId, $success) {
        if ($this->getLOG() && $this->getDB() != 'pp-log' && !empty($logId) && $success === false) {
            include_once '/var/www/puxapreco/services/common/log.php';
            $log = Log::getInstance();
            $log->setLogId($logId);
            $excluido = $log->manipulaLog('excluir');
            return $excluido;
        }
        return false;
    }

}
