<?php

class CCategories extends Controller {

   private $model;

   function __construct() {
      include_once 'MCategories.php';
      $this->model = new MCategories();
   }
   
   public function getOtherwiseInsert($categoriaId, $description, $parentId){
      $cats = $this->getCategoriaByDescription(strtolower($description));
      if(empty($cats)){
         $newCatId = $this->createNew($categoriaId, $description, $parentId);
      }else{
         $newCatId = $cats[0]['category_id'];
      }
      return $newCatId;
   }
   
   public function getCategoriaById($categoriaId) {
      return $this->model->read($categoriaId);
   }
   
   public function getChildren($parentCategoryId){
      return $this->model->read(NULL, NULL, $parentCategoryId);
   }
   
   public function getParents(){
      $parents =  $this->model->read(NULL, NULL, 1); //filhos de index
      return $parents;
   }
   public function getCategoriaByDescription($description) {
      return $this->model->read(NULL, $description);
   }

   public function createNew($categoriaId = NULL, $description = NULL,  $parentId = NULL) {
      return $this->model->create($description, $parentId, $categoriaId);
   }
   
   public function getCategoryByDescription($description){
      return $this->model->read(NULL, $description);
   }
   /**
    * Busca apenas categorias filhas, a partir de uma lista de identificadores de categorias
    * @author Douglas Haubert <dhaubert.ti@gmail.com>
    * @param array $categoriesIds Vetor com identificadores de categorias
    * @return array Apenas identificadores de categorias filhas
    */
   public function getOnlyChildren(array $categoriesIds){
      $children = array();
      foreach($categoriesIds as $categoryId){
         $subCategories = $this->getChildren($categoryId);
         if($subCategories){
            foreach($subCategories as $subCategory){
               $children[] = !in_array($subCategory['category_id'], $children)? $subCategory['category_id'] : NULL;
            }
         }else{
            $children[] = $categoryId;
         }
      }
      return $children;
      
   }
   public function getAll() {
      
      $parents = $this->getParents();
      foreach($parents as $i => $parent){
         $parents[$i]['filhos'] = $this->getChildren($parent['category_id']);
      }
      return $parents;
   }

}
