<?php

class VCategoriesComponents {

   public function selectCategories($categories, $selectedId = NULL) {
      ob_start();
      ?>
      <select id='category' name="category">
         <?php
         foreach ($categories as $category) {
            $selectedParent = ($selectedId == $category['category_id'])? "selected='selected'" : "";
            ?>
            <optgroup label='<?= $category['description'] ?>'>
               <option <?= $selectedParent; ?> value='<?= $category['category_id'] ?>'><?= $category['description'] ?></option>
               <?php
               foreach ($category['filhos'] as $child) {
                  $selectedChild = ($selectedId == $child['category_id'])? "selected='selected'" : "";
                  ?>
                  <option <?= $selectedChild; ?> value='<?= $child['category_id'] ?>'><?= $child['description'] ?></option>   
                  <?php
               }
               ?>
            </optgroup>
            <?php
         }
         ?>
      </select>
      <?php
      return ob_get_clean();
   }

}
