<?php

class MCategories extends Model{
   
   public function __construct() {
   }
   
   public function create($description, $parentId = NULL, $categoriaId = NULL){
      $this->connect();
      $this->setEncode('utf8');
      $descriptionLower = strtolower($description);
      $inserted  = $this->insertIgnore(
                        "categories",
                        "category_id, description,  parent_category_id",
                        $this->putQuotes($categoriaId). ", ". $this->putQuotes($descriptionLower). 
                         ", " . $this->putQuotes($parentId)
            );
      $callback = $inserted? $this->last_id() : false;
      $this->close();
      return $callback;
   }
   
   public function read($categoriaId = NULL, $description = NULL, $parentId  = NULL){
      $whereCategoriaId  = isset($categoriaId)? " AND category_id = {$categoriaId} " : "";
      $whereDescription  = isset($description)? " AND description LIKE '%{$description}%' " : "";
      $whereParentId  = isset($parentId)? " AND parent_category_id = '{$parentId}' " : "";
      $this->connect(); 
      $result = $this->select(
            "*",
            "categories",
            "1=1 $whereCategoriaId $whereDescription  $whereParentId
               GROUP BY description
               ORDER BY description 
               
            "
            );
      $categorias = false;
      while($result && $row = mysqli_fetch_assoc($result)){
         $categorias[] = $row;
      }
      $this->close();
      return $categorias;
   }
   
}

