<section>
   <div class="">
      <div class="row">            
         <div class="col m12 s12">
            <div class="row">
               <div class="col s12">
                  <div class="col s12 m9">
                     <h4><?= _("Gerenciamento de Usuários") ?></h4>
                  </div>
                  <div class="fixed-action-btn vertical click-to-toggle" style="bottom: 45px; right: 24px;">
                     <a class="btn-floating btn-large waves-effect waves-light red modal-trigger"  href="#modal-view" onclick="clearModal()"><i class="material-icons">add</i></a>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col s12 m12">
                  <table id="table-items" class="responsive-table hover compact nowrap stripe" data-order='[[ 0, "asc" ],[ 1, "asc"]]' data-page-length='10'>
                     <thead>
                        <tr>
                           <th ><?= _("ID") ?></th>
                           <th ><?= _("Nome") ?></th>
                           <th ><?= _("Sobrenome") ?></th>
                           <th ><?= _("Email") ?></th>
                           <th ><?= _("Ações") ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($_users as $user) { ?>
                           <tr>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewUser(<?= $user['user_id'] ?>)">
                                    <strong><?= ($user['user_id']) ?></strong>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewUser(<?= $user['user_id'] ?>)">
                                    <?= $user['first_name'] ?>
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewUser(<?= $user['user_id'] ?>)">
                                    <?= $user['last_name'] ?>  
                                 </a>
                              </td>
                              <td >
                                 <a class="modal-trigger" href="#modal-view" onclick="viewUser(<?= $user['user_id'] ?>)">
                                    <?= $user['email'] ?>
                                 </a>
                              </td>
                              <td>
                                 <a title="<?= _("Visualizar") ?>" class="waves-effect waves-light btn-floating modal-trigger" href="#modal-view" onclick="viewUser(<?= $user['user_id'] ?>)"><i class="material-icons">visibility</i></a>
                                 <a title="<?= _("Excluir") ?>" class="waves-effect waves-light btn-floating red" onclick="exclui('<?= $user['user_id']; ?>', '<?= $user['first_name'] . ' ' . $user['last_name']; ?>');">
                                    <i class="material-icons">delete</i>
                                 </a>
                              </td>
                           </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <br/>
         </div>
      </div>
   </div>
</section>
<!--- Modal --->
<div id="modal-view" class="modal modal-fixed-footer">
   <div class="modal-content">
      <div class="row">
         <form class="col s12" id='formEdition' name='formEdition'>
            <div class="row">
               <div class="input-field col s6">
                  <input  id="firstName" name="firstName" type="text" class="validate">
                  <label for="firstName"><?= _("Primeiro Nome"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="lastName" name="lastName" type="text" class="validate">
                  <label for="lastName"><?= _("Sobrenome"); ?></label>
               </div>
            </div> 
            <div class="row">
               <div class="input-field col s4">
                  <input id="email" name="email" type="text" class="validate">
                  <label for="email"><?= _("Email"); ?></label>
               </div>
               <div class="input-field col s6">
                  <input id="password" name="password" type="password" class="validate">
                  <label for="password"><?= _("Senha"); ?></label>
               </div>
            </div> 
         </form>
      </div>
   </div>
   <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat "><?= _("Fechar") ?></a>
      <a id="btn-salvar" onclick='salvar()' class="modal-action modal-close waves-effect waves-green btn "><?= _("Salvar") ?></a>
   </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
         function salvar(id) {
            var formData = $("#formEdition").serialize();
            sendAjax("action=salvar&userId=" + id + "&" + formData, function (data) {
               if (data) {
                  Materialize.toast('<?= _('Usuário salvo com sucesso') ?>', 5000, 'green', location.reload());
               } else {
                  Materialize.toast('<?= _('Falha ao salvar o usuário') ?>', 5000, 'red');
               }
            });
         }

         function exclui(id, description) {
            if (confirm("Tem certeza que deseja remover o usuário " + description + "?")) {
               sendAjax("action=remover&userId=" + id, function (data) {
                  if (data == 1) {
                     Materialize.toast('<?= _('Usuário removido com sucesso') ?>', 5000, 'green', location.reload());
                  } else if (data == 1451) {
                     Materialize.toast('<?= _('Existem coletas de preços associadas a este usuário. Exclua-as e tente novamente.') ?>', 5000, 'red');
                  } else {
                     Materialize.toast('<?= _('Falha ao remover o usuário') ?>', 5000, 'red');
                  }
               });
            }
         }
         /**
          * Envio de requisições AJAX
          * @param {String} data
          * @returns {retorno} */
         function sendAjax(data, callable) {
            $.ajax({
               'data': data
            }).done(callable);
         }
         /**
          * 
          * @param {type} id
          * @returns {undefined}
          */
         function viewUser(id) {
            sendAjax("action=listar&userId=" + id, function (data) {
               var user = data[0];
               $("#firstName").val(user.first_name);
               $("label[for='firstName']").addClass('active');
               $("#lastName").val(user.last_name);
               $("label[for='lastName']").addClass('active');
               $("#email").val(user.email);
               $("label[for='email']").addClass('active');
               $("#btn-salvar").attr('onclick', 'salvar(\'' + id + '\')');
            });
         }
         function clearModal() {
            $("#firstName").val('');
            $("label[for='firstName']").removeClass('active');
            $("#lastName").val('');
            $("label[for='lastName']").removeClass('active');
            $("#email").val('');
            $("label[for='email']").removeClass('active');
            $("#password").val('');
            $("label[for='password']").removeClass('active');
            $("#btn-salvar").attr('onclick', 'salvar(\'\')');
         }
         $(document).ready(function () {
            $('.modal-trigger').leanModal();
            $.ajaxSetup({
               'url': baseURL + "/users/ajax?",
               'dataType': 'json',
               'global': 'true',
               'type': 'post'
            });
            $("#table-items").DataTable({select: true});
         });
</script>