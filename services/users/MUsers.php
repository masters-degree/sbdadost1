<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MUsers
 *
 * @author douglas
 */
class MUsers extends Model {

   public function read($userId = NULL, $email = NULL, $password = NULL) {
      $whereUser = isset($userId) ? " AND user_id = {$userId}" : "";
      $whereEmail = isset($email) ? " AND email = {$email}" : "";
      $whereSenha = isset($password) ? " AND password = SHA2('{$password}', 512)" : "";
      $this->connect();
      $result = $this->select(
            "user_id, first_name, last_name, password, email", "users", "1 = 1 {$whereUser} {$whereEmail} {$whereSenha}"
      );
      $users = false;
      while ($row = mysqli_fetch_assoc($result)) {
         $users[] = $row;
      }
      $this->close();
      return $users;
   }

   public function create($userId = NULL, $email = NULL, $password = NULL, $firstName = NULL, $lastName = NULL) {
      $this->connect();
      $inserted = $this->insert('users', 'user_id, email, password, first_name, last_name',
            $this->putQuotes($userId) . "," .
            $this->putQuotes($email) . "," .
            "SHA2('{$password}',512),".
            $this->putQuotes($firstName) . "," .
            $this->putQuotes($lastName));
      $status = $inserted ? $this->last_id() : false;
      $this->close();
      return $status;
   }

   public function edit($userId, $email = NULL, $password = NULL, $firstName = NULL, $lastName = NULL) {
      $this->connect();
      if (isset($email)) {
         $fields[] = " email = " . $this->putQuotes($email);
      }
      if (isset($password)) {
         $fields[] = " password = SHA2('" . $this->putQuotes($password) . "', 512) ";
      }
      if (isset($firstName)) {
         $fields[] = " first_name = " . $this->putQuotes($firstName);
      }
      if (isset($lastName)) {
         $fields[] = " last_name = " . $this->putQuotes($lastName);
      }
      $ok = $this->update(
            "users ", implode(',', $fields), "user_id = {$userId} "
      );
      $this->close();
      return $ok;
   }

   public function remove($userId) {
      $this->connect();
      $status = $this->delete(
            "users ", "user_id = {$userId} "
      );
      if (!$status) {
         $lastError = $this->getLastError();
         $status = $lastError['id'];
      }
      $this->close();
      return $status;
   }

}
