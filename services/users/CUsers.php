<?php

class CUsers extends Controller{
   private $model;
   
   public function __construct(){
      $this->model = new MUsers();
   }
   public function createNew($userId = NULL, $email = NULL, $password = NULL, $firstName = NULL, $lastName = NULL){
      $status = $this->model->create($userId, $email, $password, $firstName, $lastName);
      return $status;
   }
   public function update($userId, $email = NULL, $password = NULL, $firstName = NULL, $lastName = NULL){
      $status = $this->model->edit($userId, $email, $password, $firstName, $lastName);
      return $status;
   }
   public function remove($userId){
      $status = $this->model->remove($userId);
      return $status;
   }
   public function getUserById($userId){
      $user = $this->model->read($userId);
      return $user;
   }
   public function getAll(){
      $users = $this->model->read();
      return $users;
   }
   
   public function authenticateUser($email, $password){
      $user = $this->model->read(NULL, $email, $password);
      return $user;
   }
   
   public function _index(){
      $vars['users'] = $this->getAll();
      $this->view('users', 'usersManage', $vars);
   }
}
