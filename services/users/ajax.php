<?php

// $_action vem do GET ['action'], se for nulo, vem do POST['action']

if ($_action) {
   $controller = new Cusers();
   $inputType = INPUT_POST;
   switch ($_action) {
      case 'listarTodos':
         $retorno = $controller->getAll();
         break;
      case 'listar':
         $userId = filter_input($inputType, 'userId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->getUserById($userId);
         break;
      case 'salvar': //salva e edita
         $userId = filter_input($inputType, 'userId', FILTER_SANITIZE_NUMBER_INT);
         $lastName = filter_input($inputType, 'lastName', FILTER_DEFAULT);
         $firstName = filter_input($inputType, 'firstName', FILTER_DEFAULT);
         $email = filter_input($inputType, 'email', FILTER_SANITIZE_EMAIL);
         $password = filter_input($inputType, 'password', FILTER_DEFAULT);
         if(empty($userId)){
            $retorno = $controller->createNew($userId, $email, $password, $firstName, $lastName);
         }else{
            $retorno = $controller->update($userId, $email, $password, $firstName, $lastName);
         }
         break;
      case 'remover':
         $userId = filter_input($inputType, 'userId', FILTER_SANITIZE_NUMBER_INT);
         $retorno = $controller->remove($userId);
         break;
   }
   echo json_encode($retorno);
}