-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03-Abr-2016 às 22:32
-- Versão do servidor: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `all-in`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `parent_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`category_id`, `description`, `parent_category_id`) VALUES
(2, 'Bebidas', NULL),
(3, 'Padaria', NULL),
(4, 'Feira', NULL),
(5, 'Congelados', NULL),
(6, 'Limpeza', NULL),
(7, 'Pães', 3),
(8, 'Embutidos', 3),
(9, 'Cervejas', 2),
(10, 'Refrigerantes', 2),
(11, 'Sucos', 2),
(12, 'Laticínios', 3),
(13, 'Cozinha', 6),
(14, 'Banheiro', 6),
(15, 'Carnes', 5),
(16, 'Peixes', 5),
(17, 'Frutas', 4),
(18, 'Verduras', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `content` float(7,2) DEFAULT NULL,
  `packing` varchar(45) DEFAULT NULL,
  `unity` varchar(10) DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `image_url` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `items`
--

INSERT INTO `items` (`item_id`, `description`, `category_id`, `content`, `packing`, `unity`, `brand`, `image_url`) VALUES
(1, 'Cerveja Polar', 9, 473.00, 'latao', 'ml', 'Ambev', NULL),
(2, 'Cerveja Heineken', 9, 355.00, 'long neck', 'ml', 'Heineken', NULL),
(3, 'Coca-cola', 10, 1000.00, 'garrafa 1L', 'ml', 'Coca Cola', NULL),
(4, 'Fanta', 10, 2000.00, 'garrafa 2L', 'ml', 'Coca-cola', NULL),
(5, 'Coca-cola', 10, 2000.00, 'garrafa 2L', 'ml', 'Coca-cola', NULL),
(6, 'Veja Desengordurante', 13, 500.00, 'spray', 'ml', 'Veja', 'https://media-services.digital-rb.com/s3/cdn.br.one.digital-rb.com/ProductImages/7891035215506/7891035215506_Front-1.jpg?width=1280&height=1280'),
(7, 'Veja X14', 14, 500.00, 'spray', 'ml', 'Veja', 'http://www.lojavirtualsegura.com.br/produtos/2933/201394131523_1.jpg'),
(8, 'Maçã Gala', 17, 1000.00, '1 KG', 'g', '', 'http://supermercadoonlinecuritiba.com.br/image/cache/data/Frutas/MACA%20GALA%20MIUDA%20KG-1000x1000.jpeg'),
(9, 'Detergente Ipê Neutro', 13, 500.00, '500ml', 'ml', 'Ipê', 'http://supermercadoonlinecuritiba.com.br/image/cache/data/Detergentes/DETERGENTE%20YPE%20500ML%20NEUTRO-1000x1000.jpg'),
(10, 'Pão francês Bel', 7, 1000.00, '1 KG', 'g', 'Bel', 'http://supermercadoonlinecuritiba.com.br/image/cache/data/P%C3%A3es%20e%20torradas/PAO%20BEL%20PAN%20FRANCES%20KG-1000x1000.jpeg'),
(11, 'Pão 7 grãos', 7, 300.00, 'Pacote', 'g', 'Nutrella', 'http://supermercadoonlinecuritiba.com.br/image/cache/data/P%C3%A3es%20e%20torradas/PAO%20NUTRELLA%20SLIM%20300G%207%20GRAOS-1000x1000.jpeg'),
(12, 'Iogurte Funcional 4 Morango 2 Ameixa', 12, 540.00, '6 potes', 'g', 'Frimesa', 'http://supermercadoonlinecuritiba.com.br/image/cache/data/Iogurtes/IOGURTE%20FRIMESA%20FUNCIONAL%20400G%204%20MORANGO%202%20AMEIXA-1000x1000.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `items_prices`
--

CREATE TABLE `items_prices` (
  `item_price_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `value` float(5,2) DEFAULT NULL,
  `place_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `collected_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `places`
--

CREATE TABLE `places` (
  `place_id` int(11) NOT NULL,
  `corporate_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `places`
--

INSERT INTO `places` (`place_id`, `corporate_name`, `description`, `phone`, `email`) VALUES
(1, 'Carretur Ltda', 'Carretour', '5532121458', 'contato@carretur.com.br'),
(2, 'Mall art Service SA', 'Mall Art Grande', NULL, 'grande@mallart.com.br'),
(3, 'Matuzzo Supermercados', 'Matuzzo Camobi', '5598764532', 'mattuzo@matuzzosupermercados.com.br'),
(4, 'Varejão Cia Ltda', 'Varejão', '5531319988', NULL),
(5, 'Azul Royal', 'Supermercado Azul Royal', '5512344331', 'azul@royal.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `first_name`, `last_name`) VALUES
(1, 'coletor1@allin.com', 'e6991eaf9dc2fed32aea8c47b6364e2158d7bbe69859ccde9e7bb5d9e6c921b6bf8082ee679d25d7e159b5c50a3e5824f0fb1d510b324b878b65f2171ee79494', 'Coletor', 'Um'),
(2, 'coletor2@allin.com', 'e6991eaf9dc2fed32aea8c47b6364e2158d7bbe69859ccde9e7bb5d9e6c921b6bf8082ee679d25d7e159b5c50a3e5824f0fb1d510b324b878b65f2171ee79494', 'Coletor', 'Dois'),
(3, 'coletor3@allin.com', 'e6991eaf9dc2fed32aea8c47b6364e2158d7bbe69859ccde9e7bb5d9e6c921b6bf8082ee679d25d7e159b5c50a3e5824f0fb1d510b324b878b65f2171ee79494', 'Coletor', 'Três'),
(4, 'coletor4@allin.com', 'e6991eaf9dc2fed32aea8c47b6364e2158d7bbe69859ccde9e7bb5d9e6c921b6bf8082ee679d25d7e159b5c50a3e5824f0fb1d510b324b878b65f2171ee79494', 'Coletor', 'Quatro'),
(5, 'coletor5@allin.com', 'e6991eaf9dc2fed32aea8c47b6364e2158d7bbe69859ccde9e7bb5d9e6c921b6bf8082ee679d25d7e159b5c50a3e5824f0fb1d510b324b878b65f2171ee79494', 'Coletor', 'Quinto');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `fk_items_1_idx` (`category_id`);

--
-- Indexes for table `items_prices`
--
ALTER TABLE `items_prices`
  ADD PRIMARY KEY (`item_price_id`),
  ADD KEY `fk_items_prices_1_idx` (`item_id`),
  ADD KEY `fk_items_prices_2_idx` (`place_id`),
  ADD KEY `fk_items_prices_3_idx` (`user_id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`place_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `items_prices`
--
ALTER TABLE `items_prices`
  MODIFY `item_price_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `place_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_items_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `items_prices`
--
ALTER TABLE `items_prices`
  ADD CONSTRAINT `fk_items_prices_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_items_prices_2` FOREIGN KEY (`place_id`) REFERENCES `places` (`place_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_items_prices_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
